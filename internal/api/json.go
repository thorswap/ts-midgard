package api

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math"
	"math/rand"
	"net/http"
	"net/url"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/thorchain/midgard/internal/util/midlog"

	"gitlab.com/thorchain/midgard/internal/logger"

	"gitlab.com/thorchain/midgard/internal/decimal"

	"gitlab.com/thorchain/midgard/internal/fetch/record"

	"gitlab.com/thorchain/midgard/config"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/thorchain/midgard/internal/db"
	"gitlab.com/thorchain/midgard/internal/util"
	"gitlab.com/thorchain/midgard/internal/util/miderr"

	"gitlab.com/thorchain/midgard/internal/timeseries"
	"gitlab.com/thorchain/midgard/internal/timeseries/stat"
	"gitlab.com/thorchain/midgard/openapi/generated/oapigen"
)

// Version 1 compatibility is a minimal effort attempt to provide smooth migration.

// TODO(HooriRn): this struct is not needed since the graphql depracation, replace with the corresponding oapi version. (delete-graphql)
type Health struct {
	CatchingUp    bool  `json:"catching_up"`
	Database      bool  `json:"database"`
	ScannerHeight int64 `json:"scannerHeight,string"`
}

type TCLogger struct {
	Log string
}

var (
	allLogs      []TCLogger
	allLogsLock  sync.Mutex
	unitLock     sync.Mutex
	currentUnits map[string]int64
)

func init() {
	allLogs = make([]TCLogger, 0)
	currentUnits = make(map[string]int64)
	go refreshPoolUnits()
	go refreshSwapHistory()
}

type sampleResp struct {
	headers map[string][]string
}

func (s sampleResp) Header() http.Header {
	return s.headers
}

func (s sampleResp) Write(bytes []byte) (int, error) {
	return 0, nil
}

func (s sampleResp) WriteHeader(statusCode int) {
}

func refreshSwapHistory() {
	time.Sleep(2 * time.Minute)
	for {
		synced := db.FullyCaughtUp()
		if !synced {
			time.Sleep(20 * time.Second)
			continue
		}
		resp := sampleResp{
			headers: make(map[string][]string),
		}
		logger.AddLog("Try swaps for 400 days")
		req, _ := http.NewRequestWithContext(context.Background(), "GET", "/v2/history/swaps?interval=day&count=400", nil)
		jsonSwapHistory(resp, req, nil)
		logger.AddLog("End of swaps for 400 days")

		logger.AddLog("Try swaps for 100 month")
		req, _ = http.NewRequestWithContext(context.Background(), "GET", "/v2/history/swaps?interval=month&count=100", nil)
		jsonSwapHistory(resp, req, nil)
		logger.AddLog("End of swaps for 100 month")

		logger.AddLog("Try unique swaps for 400 days")
		req, _ = http.NewRequestWithContext(context.Background(), "GET", "/v2/history/swaps?interval=day&count=400&unique=true", nil)
		jsonSwapHistory(resp, req, nil)
		logger.AddLog("End of unique swaps for 400 days")

		logger.AddLog("Try unique swaps for 100 month")
		req, _ = http.NewRequestWithContext(context.Background(), "GET", "/v2/history/swaps?interval=month&count=100&unique=true", nil)
		jsonSwapHistory(resp, req, nil)
		logger.AddLog("End of unique swaps for 100 month")

		logger.AddLog("Try ts-swaps for 400 days")
		req, _ = http.NewRequestWithContext(context.Background(), "GET", "/v2/history/ts-swaps?interval=day&count=400", nil)
		jsonTsSwapHistory(resp, req, nil)
		logger.AddLog("End of ts-swaps for 400 days")

		logger.AddLog("Try ts-swaps for 100 month")
		req, _ = http.NewRequestWithContext(context.Background(), "GET", "/v2/history/ts-swaps?interval=month&count=100", nil)
		jsonTsSwapHistory(resp, req, nil)
		logger.AddLog("End of ts-swaps for 100 month")

		logger.AddLog("Try unique ts-swaps for 400 days")
		req, _ = http.NewRequestWithContext(context.Background(), "GET", "/v2/history/ts-swaps?interval=day&count=400&unique=true", nil)
		jsonTsSwapHistory(resp, req, nil)
		logger.AddLog("End of unique ts-swaps for 400 days")

		logger.AddLog("Try unique ts-swaps for 100 month")
		req, _ = http.NewRequestWithContext(context.Background(), "GET", "/v2/history/ts-swaps?interval=month&count=100&unique=true", nil)
		jsonTsSwapHistory(resp, req, nil)
		logger.AddLog("End of unique ts-swaps for 100 month")

		time.Sleep(10 * time.Minute)
	}
}

func getCurrentUnit() map[string]int64 {
	unitLock.Lock()
	defer unitLock.Unlock()
	return currentUnits
}

func refreshPoolUnits() {
	for {
		synced := db.FullyCaughtUp()
		if !synced {
			time.Sleep(time.Second * 10)
			continue
		}
		blockStat := timeseries.Latest.GetState()
		pools := make([]string, 0)
		for k := range blockStat.Pools {
			pools = append(pools, k)
		}
		if blockStat.Pools == nil || len(blockStat.Pools) == 0 {
			time.Sleep(time.Second * 5)
			continue
		}
		liquidityUnitsNow, err := stat.CurrentPoolsLiquidityUnits(context.Background(), pools)
		if err != nil {
			time.Sleep(time.Second * 3)
		} else {
			unitLock.Lock()
			currentUnits = liquidityUnitsNow
			unitLock.Unlock()
			time.Sleep(time.Second * 5)
		}
	}
}

func AddLog(log string) {
	return
	allLogsLock.Lock()
	defer allLogsLock.Unlock()
	if len(allLogs) > 3000 {
		allLogs = make([]TCLogger, 0)
	}
	allLogs = append(allLogs, TCLogger{
		log,
	})
}

/*func jsonLogs(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	allLogsLock.Lock()
	defer allLogsLock.Unlock()
	respJSON(w, allLogs)
}*/

func jsonHealth(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	merr := util.CheckUrlEmpty(r.URL.Query())
	if merr != nil {
		merr.ReportHTTP(w)
		return
	}

	height, _, _ := timeseries.LastBlock()
	synced := db.FullyCaughtUp()
	_ = synced
	resp := oapigen.HealthResponse{
		InSync:         true,
		Database:       true,
		ScannerHeight:  util.IntStr(height + 1),
		LastThorNode:   db.LastThorNodeBlock.AsHeightTS(),
		LastFetched:    db.LastFetchedBlock.AsHeightTS(),
		LastCommitted:  db.LastCommittedBlock.AsHeightTS(),
		LastAggregated: db.LastAggregatedBlock.AsHeightTS(),
	}
	bt, _ := json.Marshal(resp)
	if rand.Intn(100) > 30 {
		midlog.InfoF("Health: %s", string(bt))
	}

	respJSON(w, resp)
}

func jsonEarningsHistory(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	f := func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		urlParams := r.URL.Query()
		buckets, merr := db.BucketsFromQuery(r.Context(), &urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		merr = util.CheckUrlEmpty(urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		var res oapigen.EarningsHistoryResponse
		res, err := stat.GetEarningsHistory(r.Context(), buckets)
		if err != nil {
			miderr.InternalErrE(err).ReportHTTP(w)
			return
		}
		if buckets.OneInterval() {
			res.Intervals = oapigen.EarningsHistoryIntervals{}
		}
		respJSON(w, res)
	}
	GlobalApiCacheStore.Get(GlobalApiCacheStore.LongTermLifetime, f, w, r, params)
}

func jsonLiquidityHistory(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	f := func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
		urlParams := r.URL.Query()
		/*from:=util.ConsumeUrlParam(&urlParams, "from")
		to:=util.ConsumeUrlParam(&urlParams, "to")
		count:=util.ConsumeUrlParam(&urlParams,"count")
		interval:=util.ConsumeUrlParam(&urlParams,"interval")
		if from=="" && to=="" && interval=="day" && (count=="10" || count=="100") {
			if poolLiquidityChangesJob.response.buf.Len()>0{
				var res oapigen.LiquidityHistoryResponse
				err:=json.Unmarshal(poolLiquidityChangesJob.response.buf.Bytes(),&res)
				if err!=nil{
					res.Intervals
				}
			}
		}*/
		buckets, merr := db.BucketsFromQuery(r.Context(), &urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		pool := util.ConsumeUrlParam(&urlParams, "pool")
		if pool == "" {
			pool = "*"
		}
		merr = util.CheckUrlEmpty(urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		var res oapigen.LiquidityHistoryResponse
		res, err := stat.GetLiquidityHistory(r.Context(), buckets, pool)
		if err != nil {
			miderr.InternalErrE(err).ReportHTTP(w)
			return
		}
		if buckets.OneInterval() {
			res.Intervals = oapigen.LiquidityHistoryIntervals{}
		}
		respJSON(w, res)
	}
	GlobalApiCacheStore.Get(GlobalApiCacheStore.LongTermLifetime, f, w, r, params)
}

func getPoolHistories(pool string, buckets db.Buckets, ctx context.Context) (
	beforeDepth timeseries.PoolDepths, depths []stat.PoolDepthBucket,
	beforeLPUnits int64, units []stat.UnitsBucket, beforeMemberCount int64, memberCounts []stat.CountBucket, err error,
) {
	beforeDepth, depths, err = stat.PoolDepthHistory(ctx, buckets, pool)
	if err != nil {
		return
	}
	beforeLPUnits, units, err = stat.PoolLiquidityUnitsHistory(ctx, buckets, pool)
	if err != nil {
		return
	}
	beforeMemberCount, memberCounts, err = stat.GetMembersCountBucket(ctx, buckets, pool)
	if err != nil {
		return
	}
	if len(depths) != len(units) || depths[0].Window != units[0].Window || len(memberCounts) != len(units) || memberCounts[0].Window != units[0].Window {
		return
	}

	return
}

func jsonDepths(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	f := func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
		pool := params[0].Value

		if !timeseries.PoolExists(pool) {
			miderr.BadRequestF("Unknown pool: %s", pool).ReportHTTP(w)
			return
		}

		urlParams := r.URL.Query()
		buckets, merr := db.BucketsFromQuery(r.Context(), &urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		merr = util.CheckUrlEmpty(urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		beforeDepth, depths, beforeLPUnits, units, beforeMemberCount, memberCounts, err := getPoolHistories(pool, buckets, r.Context())
		if err != nil {
			respError(w, err)
		}
		var result oapigen.DepthHistoryResponse = toOapiDepthResponse(
			r.Context(), beforeDepth, depths, beforeLPUnits, units, beforeMemberCount, memberCounts)
		respJSON(w, result)
	}
	GlobalApiCacheStore.Get(GlobalApiCacheStore.LongTermLifetime, f, w, r, params)
}

func jsonSaversDepths(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	pool := ps[0].Value
	saverVaultName := util.ConvertNativePoolToSynth(pool)

	if !timeseries.PoolExists(saverVaultName) {
		miderr.BadRequestF("Unknown saver pool: %s", saverVaultName).ReportHTTP(w)
		return
	}

	urlParams := r.URL.Query()
	buckets, merr := db.BucketsFromQuery(r.Context(), &urlParams)
	if merr != nil {
		merr.ReportHTTP(w)
		return
	}

	merr = util.CheckUrlEmpty(urlParams)
	if merr != nil {
		merr.ReportHTTP(w)
		return
	}

	beforeDepth, depths, beforeUnits, units, beforeMemberCount, memberCounts, err := getPoolHistories(saverVaultName, buckets, r.Context())
	if err != nil {
		respError(w, err)
	}

	var result oapigen.SaversHistoryResponse = toOapiSaversHistoryResponse(
		r.Context(), beforeDepth, depths, beforeUnits, units, beforeMemberCount, memberCounts)
	respJSON(w, result)
}

func toOapiSaversHistoryResponse(
	ctx context.Context,
	beforeDepth timeseries.PoolDepths,
	depths []stat.PoolDepthBucket,
	beforeUnits int64,
	units []stat.UnitsBucket,
	beforeMemberCount int64,
	memberCounts []stat.CountBucket) (
	result oapigen.SaversHistoryResponse,
) {
	result.Intervals = make(oapigen.SaversHistoryIntervals, 0, len(depths))
	for i, bucket := range depths {
		if len(units) <= i {
			break
		}
		liquidityUnits := units[i].Units
		membersCount := memberCounts[i].Count
		assetDepth := bucket.Depths.AssetDepth
		result.Intervals = append(result.Intervals, oapigen.SaversHistoryItem{
			StartTime:   util.IntStr(bucket.Window.From.ToI()),
			EndTime:     util.IntStr(bucket.Window.Until.ToI()),
			SaversDepth: util.IntStr(assetDepth),
			SaversUnits: util.IntStr(liquidityUnits),
			SaversCount: util.IntStr(membersCount),
		})
	}
	if len(depths) == 0 || len(units) == 0 || len(memberCounts) == 0 {
		return
	}
	var endDepth timeseries.PoolDepths
	var endUnits int64
	var endCount int64
	if len(depths) > 0 {
		endDepth = depths[len(depths)-1].Depths
	}
	if len(units) > 0 {
		endUnits = units[len(units)-1].Units
	}
	if len(memberCounts) > 0 {
		endCount = memberCounts[len(memberCounts)-1].Count
	}

	result.Meta.StartTime = util.IntStr(depths[0].Window.From.ToI())
	result.Meta.EndTime = util.IntStr(depths[len(depths)-1].Window.Until.ToI())
	result.Meta.StartSaversDepth = util.IntStr(beforeDepth.AssetDepth)
	result.Meta.StartUnits = util.IntStr(beforeUnits)
	result.Meta.StartSaversCount = util.IntStr(beforeMemberCount)
	result.Meta.EndSaversDepth = util.IntStr(endDepth.AssetDepth)
	result.Meta.EndUnits = util.IntStr(endUnits)
	result.Meta.EndSaversCount = util.IntStr(endCount)
	return
}

func toOapiDepthResponse(
	ctx context.Context,
	beforeDepth timeseries.PoolDepths,
	depths []stat.PoolDepthBucket,
	beforeLPUnits int64,
	units []stat.UnitsBucket,
	beforeMemberCount int64,
	memberCounts []stat.CountBucket) (
	result oapigen.DepthHistoryResponse,
) {
	result.Intervals = make(oapigen.DepthHistoryIntervals, 0, len(depths))
	for i, bucket := range depths {
		if len(units) <= i {
			break
		}
		liquidityUnits := units[i].Units
		membersCount := memberCounts[i].Count
		synthUnits := timeseries.CalculateSynthUnits(
			bucket.Depths.AssetDepth, bucket.Depths.SynthDepth, liquidityUnits)
		poolUnits := liquidityUnits + synthUnits
		assetDepth := bucket.Depths.AssetDepth
		runeDepth := bucket.Depths.RuneDepth
		liqUnitValIndex := luvi(bucket.Depths.AssetDepth, bucket.Depths.RuneDepth, poolUnits)
		result.Intervals = append(result.Intervals, oapigen.DepthHistoryItem{
			StartTime:      util.IntStr(bucket.Window.From.ToI()),
			EndTime:        util.IntStr(bucket.Window.Until.ToI()),
			AssetDepth:     util.IntStr(assetDepth),
			RuneDepth:      util.IntStr(runeDepth),
			AssetPrice:     floatStr(bucket.Depths.AssetPrice()),
			AssetPriceUSD:  floatStr(bucket.AssetPriceUSD),
			LiquidityUnits: util.IntStr(liquidityUnits),
			SynthUnits:     util.IntStr(synthUnits),
			SynthSupply:    util.IntStr(bucket.Depths.SynthDepth),
			Units:          util.IntStr(poolUnits),
			MembersCount:   util.IntStr(membersCount),
			Luvi:           floatStr(liqUnitValIndex),
		})
	}
	if len(depths) == 0 || len(units) == 0 || len(memberCounts) == 0 {
		return
	}
	var endDepth timeseries.PoolDepths
	var endLPUnits int64
	if len(depths) > 0 {
		endDepth = depths[len(depths)-1].Depths
	}
	if len(units) > 0 {
		endLPUnits = units[len(units)-1].Units
	}

	endMemberCounts := memberCounts[len(memberCounts)-1].Count
	beforeSynthUnits := timeseries.CalculateSynthUnits(
		beforeDepth.AssetDepth, beforeDepth.SynthDepth, beforeLPUnits)
	endSynthUnits := timeseries.CalculateSynthUnits(
		endDepth.AssetDepth, endDepth.SynthDepth, endLPUnits)
	luviIncrease := luviFromLPUnits(endDepth, endLPUnits) / luviFromLPUnits(beforeDepth, beforeLPUnits)

	result.Meta.StartTime = util.IntStr(depths[0].Window.From.ToI())
	result.Meta.EndTime = util.IntStr(depths[len(depths)-1].Window.Until.ToI())
	result.Meta.PriceShiftLoss = floatStr(priceShiftLoss(beforeDepth, endDepth))
	result.Meta.LuviIncrease = floatStr(luviIncrease)
	result.Meta.StartAssetDepth = util.IntStr(beforeDepth.AssetDepth)
	result.Meta.StartRuneDepth = util.IntStr(beforeDepth.RuneDepth)
	result.Meta.StartLPUnits = util.IntStr(beforeLPUnits)
	result.Meta.StartMemberCount = util.IntStr(beforeMemberCount)
	result.Meta.StartSynthUnits = util.IntStr(beforeSynthUnits)
	result.Meta.EndAssetDepth = util.IntStr(endDepth.AssetDepth)
	result.Meta.EndRuneDepth = util.IntStr(endDepth.RuneDepth)
	result.Meta.EndLPUnits = util.IntStr(endLPUnits)
	result.Meta.EndMemberCount = util.IntStr(endMemberCounts)
	result.Meta.EndSynthUnits = util.IntStr(endSynthUnits)
	return
}

func luvi(assetE8 int64, runeE8 int64, poolUnits int64) float64 {
	if poolUnits <= 0 {
		return math.NaN()
	}
	return math.Sqrt(float64(assetE8)*float64(runeE8)) / float64(poolUnits)
}

func luviFromLPUnits(depths timeseries.PoolDepths, lpUnits int64) float64 {
	synthUnits := timeseries.CalculateSynthUnits(depths.AssetDepth, depths.SynthDepth, lpUnits)
	return luvi(depths.AssetDepth, depths.RuneDepth, lpUnits+synthUnits)
}

func priceShiftLoss(beforeDepth timeseries.PoolDepths, lastDepth timeseries.PoolDepths) float64 {
	// Price0 = R0 / A0 (rune depth at time 0, asset depth at time 0)
	// Price1 = R1 / A1 (rune depth at time 1, asset depth at time 1)
	// PriceShift = Price1 / Price0
	// PriceShiftLoss = 2*sqrt(PriceShift) / (1 + PriceShift)
	price0 := float64(beforeDepth.RuneDepth) / float64(beforeDepth.AssetDepth)
	price1 := float64(lastDepth.RuneDepth) / float64(lastDepth.AssetDepth)
	ratio := price1 / price0
	return 2 * math.Sqrt(ratio) / (1 + ratio)
}

func GetPoolAPRs(ctx context.Context,
	depthsNow timeseries.DepthMap, lpUnitsNow map[string]int64, pools []string,
	aprStartTime db.Nano, now db.Nano) (
	map[string]float64, error,
) {
	var periodsPerYear float64 = 365 * 24 * 60 * 60 * 1e9 / float64(now-aprStartTime)
	liquidityUnitsBefore, err := stat.PoolsLiquidityUnitsBefore(ctx, pools, &aprStartTime)
	if err != nil {
		return nil, err
	}
	depthsBefore, err := stat.DepthsBefore(ctx, pools, aprStartTime)
	if err != nil {
		return nil, err
	}

	ret := map[string]float64{}
	for _, pool := range pools {
		if record.GetCoinType([]byte(pool)) == record.AssetNative {
			luviNow := luviFromLPUnits(depthsNow[pool], lpUnitsNow[pool])
			luviBefore := luviFromLPUnits(depthsBefore[pool], liquidityUnitsBefore[pool])
			luviIncrease := luviNow / luviBefore
			ret[pool] = (luviIncrease - 1) * periodsPerYear
		} else {
			_, unitNowOk := lpUnitsNow[pool]
			_, unitBeforeOk := liquidityUnitsBefore[pool]
			if !unitBeforeOk || !unitNowOk {
				ret[pool] = 0.00
				continue
			}
			swapGrowthIndexBefore := float64(depthsBefore[pool].AssetDepth) / float64(liquidityUnitsBefore[pool])
			swapGrowthIndexNow := float64(depthsNow[pool].AssetDepth) / float64(lpUnitsNow[pool])
			ret[pool] = ((swapGrowthIndexNow - swapGrowthIndexBefore) / swapGrowthIndexBefore) * periodsPerYear
		}
	}
	return ret, nil
}

func GetSinglePoolAPR(ctx context.Context,
	depths timeseries.PoolDepths, lpUnits int64, pool string, start db.Nano, now db.Nano) (
	float64, error,
) {
	aprs, err := GetPoolAPRs(
		ctx,
		timeseries.DepthMap{pool: depths},
		map[string]int64{pool: lpUnits},
		[]string{pool},
		start, now)
	if err != nil {
		return 0, err
	}
	return aprs[pool], nil
}

func toOapiOhlcvResponse(
	depths []stat.OHLCVBucket) (
	result oapigen.OHLCVHistoryResponse,
) {
	result.Intervals = make(oapigen.OHLCVHistoryIntervals, 0, len(depths))
	for _, bucket := range depths {
		minDate := bucket.Depths.MinDate
		maxDate := bucket.Depths.MaxDate
		if bucket.Depths.MinDate < bucket.Window.From.ToI() {
			minDate = bucket.Window.From.ToI()
		}
		if bucket.Depths.MinDate > bucket.Window.Until.ToI() {
			minDate = bucket.Window.Until.ToI()
		}
		if bucket.Depths.MaxDate < bucket.Window.From.ToI() {
			maxDate = bucket.Window.From.ToI()
		}
		if bucket.Depths.MaxDate > bucket.Window.Until.ToI() {
			maxDate = bucket.Window.Until.ToI()
		}
		result.Intervals = append(result.Intervals, oapigen.OHLCVHistoryItem{
			ClosePrice: floatStr(bucket.Depths.LastPrice),
			CloseTime:  util.IntStr(bucket.Window.Until.ToI()),
			HighPrice:  floatStr(bucket.Depths.MaxPrice),
			HighTime:   util.IntStr(maxDate),
			Liquidity:  util.IntStr(bucket.Depths.Liquidity),
			LowPrice:   floatStr(bucket.Depths.MinPrice),
			LowTime:    util.IntStr(minDate),
			OpenPrice:  floatStr(bucket.Depths.FirstPrice),
			OpenTime:   util.IntStr(bucket.Window.From.ToI()),
			Volume:     util.IntStr(bucket.Depths.Volume),
		})
	}
	result.Meta.StartTime = util.IntStr(depths[0].Window.From.ToI())
	result.Meta.EndTime = util.IntStr(depths[len(depths)-1].Window.Until.ToI())
	return
}

func jsonSwapHistory(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	f := func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
		urlParams := r.URL.Query()

		buckets, merr := db.BucketsFromQuery(r.Context(), &urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		var pool *string
		poolParam := util.ConsumeUrlParam(&urlParams, "pool")
		if poolParam != "" {
			pool = &poolParam
		}

		var unique bool
		uniqueParam := util.ConsumeUrlParam(&urlParams, "unique")
		if uniqueParam != "" && uniqueParam == "true" {
			unique = true
		}

		merr = util.CheckUrlEmpty(urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		mergedPoolSwaps, err := stat.GetPoolSwaps(r.Context(), pool, buckets, false, false)
		if err != nil {
			miderr.InternalErr(err.Error()).ReportHTTP(w)
			return
		}
		if unique {
			uniquePoolSwaps, err := stat.GetPoolSwaps(r.Context(), pool, buckets, unique, false)
			if err != nil {
				logger.AddLog(err.Error())
				miderr.InternalErr(err.Error()).ReportHTTP(w)
				return
			}
			for _, uniqueSwap := range uniquePoolSwaps {
				for i, swap := range mergedPoolSwaps {
					if uniqueSwap.StartTime == swap.StartTime && uniqueSwap.EndTime == swap.EndTime {
						mergedPoolSwaps[i].RuneToAssetCount -= uniqueSwap.RuneToAssetCount
						mergedPoolSwaps[i].AssetToRuneCount -= uniqueSwap.AssetToRuneCount
						mergedPoolSwaps[i].RuneToSynthCount -= uniqueSwap.RuneToSynthCount
						mergedPoolSwaps[i].SynthToRuneCount -= uniqueSwap.SynthToRuneCount
						mergedPoolSwaps[i].TotalCount -= uniqueSwap.TotalCount
						mergedPoolSwaps[i].RuneToAssetVolume -= uniqueSwap.RuneToAssetVolume
						mergedPoolSwaps[i].AssetToRuneVolume -= uniqueSwap.AssetToRuneVolume
						mergedPoolSwaps[i].RuneToSynthVolume -= uniqueSwap.RuneToSynthVolume
						mergedPoolSwaps[i].SynthToRuneVolume -= uniqueSwap.SynthToRuneVolume
						mergedPoolSwaps[i].TotalVolume -= uniqueSwap.TotalVolume
						mergedPoolSwaps[i].TotalVolumeUsd -= uniqueSwap.TotalVolumeUsd
						mergedPoolSwaps[i].RuneToAssetFees -= uniqueSwap.RuneToAssetFees
						mergedPoolSwaps[i].AssetToRuneFees -= uniqueSwap.AssetToRuneFees
						mergedPoolSwaps[i].RuneToSynthFees -= uniqueSwap.RuneToSynthFees
						mergedPoolSwaps[i].SynthToRuneFees -= uniqueSwap.SynthToRuneFees
						mergedPoolSwaps[i].TotalFees -= uniqueSwap.TotalFees
						mergedPoolSwaps[i].RuneToAssetSlip -= uniqueSwap.RuneToAssetSlip
						mergedPoolSwaps[i].AssetToRuneSlip -= uniqueSwap.AssetToRuneSlip
						mergedPoolSwaps[i].RuneToSynthSlip -= uniqueSwap.RuneToSynthSlip
						mergedPoolSwaps[i].SynthToRuneSlip -= uniqueSwap.SynthToRuneSlip
						mergedPoolSwaps[i].TotalSlip -= uniqueSwap.TotalSlip
					}
				}
			}
		}
		var result oapigen.SwapHistoryResponse = createVolumeIntervals(mergedPoolSwaps)
		if buckets.OneInterval() {
			result.Intervals = oapigen.SwapHistoryIntervals{}
		}
		respJSON(w, result)
	}
	GlobalApiCacheStore.Get(GlobalApiCacheStore.LongTermLifetime, f, w, r, params)
}

func jsonTsSwapHistory(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	f := func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
		urlParams := r.URL.Query()

		buckets, merr := db.BucketsFromQuery(r.Context(), &urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		var pool *string
		poolParam := util.ConsumeUrlParam(&urlParams, "pool")
		if poolParam != "" {
			pool = &poolParam
		}

		var unique bool
		uniqueParam := util.ConsumeUrlParam(&urlParams, "unique")
		if uniqueParam != "" && uniqueParam == "true" {
			unique = true
		}

		merr = util.CheckUrlEmpty(urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		mergedPoolSwaps, err := stat.GetPoolSwaps(r.Context(), pool, buckets, false, true)
		if err != nil {
			miderr.InternalErr(err.Error()).ReportHTTP(w)
			return
		}
		if unique {
			uniquePoolSwaps, err := stat.GetPoolSwaps(r.Context(), pool, buckets, unique, true)
			if err != nil {
				miderr.InternalErr(err.Error()).ReportHTTP(w)
				return
			}
			for _, uniqueSwap := range uniquePoolSwaps {
				for i, swap := range mergedPoolSwaps {
					if uniqueSwap.StartTime == swap.StartTime && uniqueSwap.EndTime == swap.EndTime {
						mergedPoolSwaps[i].RuneToAssetCount -= uniqueSwap.RuneToAssetCount
						mergedPoolSwaps[i].AssetToRuneCount -= uniqueSwap.AssetToRuneCount
						mergedPoolSwaps[i].RuneToSynthCount -= uniqueSwap.RuneToSynthCount
						mergedPoolSwaps[i].SynthToRuneCount -= uniqueSwap.SynthToRuneCount
						mergedPoolSwaps[i].TotalCount -= uniqueSwap.TotalCount
						mergedPoolSwaps[i].RuneToAssetVolume -= uniqueSwap.RuneToAssetVolume
						mergedPoolSwaps[i].AssetToRuneVolume -= uniqueSwap.AssetToRuneVolume
						mergedPoolSwaps[i].RuneToSynthVolume -= uniqueSwap.RuneToSynthVolume
						mergedPoolSwaps[i].SynthToRuneVolume -= uniqueSwap.SynthToRuneVolume
						mergedPoolSwaps[i].TotalVolume -= uniqueSwap.TotalVolume
						mergedPoolSwaps[i].TotalVolumeUsd -= uniqueSwap.TotalVolumeUsd
						mergedPoolSwaps[i].RuneToAssetFees -= uniqueSwap.RuneToAssetFees
						mergedPoolSwaps[i].AssetToRuneFees -= uniqueSwap.AssetToRuneFees
						mergedPoolSwaps[i].RuneToSynthFees -= uniqueSwap.RuneToSynthFees
						mergedPoolSwaps[i].SynthToRuneFees -= uniqueSwap.SynthToRuneFees
						mergedPoolSwaps[i].TotalFees -= uniqueSwap.TotalFees
						mergedPoolSwaps[i].RuneToAssetSlip -= uniqueSwap.RuneToAssetSlip
						mergedPoolSwaps[i].AssetToRuneSlip -= uniqueSwap.AssetToRuneSlip
						mergedPoolSwaps[i].RuneToSynthSlip -= uniqueSwap.RuneToSynthSlip
						mergedPoolSwaps[i].SynthToRuneSlip -= uniqueSwap.SynthToRuneSlip
						mergedPoolSwaps[i].TotalSlip -= uniqueSwap.TotalSlip
					}
				}
			}
		}

		var result oapigen.SwapHistoryResponse = createVolumeIntervals(mergedPoolSwaps)
		if buckets.OneInterval() {
			result.Intervals = oapigen.SwapHistoryIntervals{}
		}
		respJSON(w, result)
	}
	GlobalApiCacheStore.Get(GlobalApiCacheStore.LongTermLifetime, f, w, r, params)
}

func toSwapHistoryItem(bucket stat.SwapBucket) oapigen.SwapHistoryItem {
	return oapigen.SwapHistoryItem{
		StartTime:              util.IntStr(bucket.StartTime.ToI()),
		EndTime:                util.IntStr(bucket.EndTime.ToI()),
		ToAssetVolume:          util.IntStr(bucket.RuneToAssetVolume),
		ToRuneVolume:           util.IntStr(bucket.AssetToRuneVolume),
		SynthMintVolume:        util.IntStr(bucket.RuneToSynthVolume),
		SynthRedeemVolume:      util.IntStr(bucket.SynthToRuneVolume),
		TotalVolume:            util.IntStr(bucket.TotalVolume),
		TotalVolumeUsd:         util.IntStr(bucket.TotalVolumeUsd),
		ToAssetCount:           util.IntStr(bucket.RuneToAssetCount),
		ToRuneCount:            util.IntStr(bucket.AssetToRuneCount),
		SynthMintCount:         util.IntStr(bucket.RuneToSynthCount),
		SynthRedeemCount:       util.IntStr(bucket.SynthToRuneCount),
		TotalCount:             util.IntStr(bucket.TotalCount),
		ToAssetFees:            util.IntStr(bucket.RuneToAssetFees),
		ToRuneFees:             util.IntStr(bucket.AssetToRuneFees),
		SynthMintFees:          util.IntStr(bucket.RuneToSynthFees),
		SynthRedeemFees:        util.IntStr(bucket.SynthToRuneFees),
		TotalFees:              util.IntStr(bucket.TotalFees),
		ToAssetAverageSlip:     ratioStr(bucket.RuneToAssetSlip, bucket.RuneToAssetCount),
		ToRuneAverageSlip:      ratioStr(bucket.AssetToRuneSlip, bucket.AssetToRuneCount),
		SynthMintAverageSlip:   ratioStr(bucket.RuneToSynthSlip, bucket.RuneToSynthCount),
		SynthRedeemAverageSlip: ratioStr(bucket.SynthToRuneSlip, bucket.SynthToRuneCount),
		AverageSlip:            ratioStr(bucket.TotalSlip, bucket.TotalCount),
		RunePriceUSD:           floatStr(bucket.RunePriceUSD),
	}
}

func createVolumeIntervals(buckets []stat.SwapBucket) (result oapigen.SwapHistoryResponse) {
	metaBucket := stat.SwapBucket{}

	for _, bucket := range buckets {
		metaBucket.AddBucket(bucket)

		result.Intervals = append(result.Intervals, toSwapHistoryItem(bucket))
	}

	result.Meta = toSwapHistoryItem(metaBucket)
	result.Meta.StartTime = result.Intervals[0].StartTime
	result.Meta.EndTime = result.Intervals[len(result.Intervals)-1].EndTime
	result.Meta.RunePriceUSD = result.Intervals[len(result.Intervals)-1].RunePriceUSD
	return
}

// TODO(huginn): remove when bonds are fixed
var ShowBonds bool = false

func jsonTVLHistory(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	f := func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
		urlParams := r.URL.Query()

		buckets, merr := db.BucketsFromQuery(r.Context(), &urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}
		merr = util.CheckUrlEmpty(urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		// TODO(huginn): optimize, just this call is 1.8 sec
		// defer timer.Console("tvlDepthSingle")()
		depths, err := stat.TVLDepthHistory(r.Context(), buckets)
		if err != nil {
			miderr.InternalErrE(err).ReportHTTP(w)
			return
		}

		bonds, err := stat.BondsHistory(r.Context(), buckets)
		if err != nil {
			miderr.InternalErrE(err).ReportHTTP(w)
			return
		}
		if len(depths) != len(bonds) || depths[0].Window != bonds[0].Window {
			miderr.InternalErr("Buckets misalligned").ReportHTTP(w)
			return
		}

		var result oapigen.TVLHistoryResponse = toTVLHistoryResponse(depths, bonds)
		respJSON(w, result)
	}
	GlobalApiCacheStore.Get(GlobalApiCacheStore.LongTermLifetime, f, w, r, params)
}

func toTVLHistoryResponse(depths []stat.TVLDepthBucket, bonds []stat.BondBucket) (
	result oapigen.TVLHistoryResponse,
) {
	showBonds := func(value string) *string {
		if !ShowBonds {
			return nil
		}
		return &value
	}

	result.Intervals = make(oapigen.TVLHistoryIntervals, 0, len(depths))
	for i, bucket := range depths {
		pools := 2 * bucket.TotalPoolDepth
		bonds := bonds[i].Bonds
		poolsDepth := toOapiPoolsDepth(bucket.PoolsMapRuneDepth)
		result.Intervals = append(result.Intervals, oapigen.TVLHistoryItem{
			StartTime:        util.IntStr(bucket.Window.From.ToI()),
			EndTime:          util.IntStr(bucket.Window.Until.ToI()),
			TotalValuePooled: util.IntStr(pools),
			TotalValueBonded: showBonds(util.IntStr(bonds)),
			TotalValueLocked: showBonds(util.IntStr(pools + bonds)),
			RunePriceUSD:     floatStr(bucket.RunePriceUSD),
			PoolsDepth:       poolsDepth,
		})
	}
	result.Meta = result.Intervals[len(depths)-1]
	result.Meta.StartTime = result.Intervals[0].StartTime
	return
}

func toOapiPoolsDepth(poolsMapDepth map[string]int64) []oapigen.DepthHistoryItemPool {
	ret := make([]oapigen.DepthHistoryItemPool, 0)
	for poolName, poolDepth := range poolsMapDepth {
		ret = append(ret, oapigen.DepthHistoryItemPool{
			Pool:       poolName,
			TotalDepth: util.IntStr(2 * poolDepth),
		})
	}
	return ret
}

func jsonNetwork(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	f := func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
		merr := util.CheckUrlEmpty(r.URL.Query())
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		network, err := timeseries.GetNetworkData(r.Context())
		if err != nil {
			respError(w, err)
			return
		}

		respJSON(w, network)
	}
	GlobalApiCacheStore.Get(GlobalApiCacheStore.MidTermLifetime, f, w, r, params)
}

type Node struct {
	Secp256K1 string `json:"secp256k1"`
	Ed25519   string `json:"ed25519"`
}

func jsonNodes(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	secpAddrs, edAddrs, err := timeseries.NodesSecpAndEd(r.Context(), time.Now())
	if err != nil {
		respError(w, err)
		return
	}

	m := make(map[string]struct {
		Secp string
		Ed   string
	}, len(secpAddrs))
	for key, addr := range secpAddrs {
		e := m[addr]
		e.Secp = key
		m[addr] = e
	}
	for key, addr := range edAddrs {
		e := m[addr]
		e.Ed = key
		m[addr] = e
	}

	array := make([]oapigen.Node, 0, len(m))
	for key, e := range m {
		array = append(array, oapigen.Node{
			Secp256k1:   e.Secp,
			Ed25519:     e.Ed,
			NodeAddress: key,
		})
	}
	respJSON(w, array)
}

func jsonLogs(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	respJSON(w, logger.GetLogs())
}

func jsonKnownPools(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	urlParams := r.URL.Query()
	merr := util.CheckUrlEmpty(urlParams)
	if merr != nil {
		merr.ReportHTTP(w)
		return
	}

	lastTime := timeseries.Latest.GetState().Timestamp
	pools, err := timeseries.GetPoolsStatuses(r.Context(), lastTime)
	if err != nil {
		respError(w, err)
		return
	}

	respJSON(w, oapigen.KnownPools{AdditionalProperties: pools})
}

// Filters out Suspended pools.
// If there is a status url parameter then returns pools with that status only.
func poolsWithRequestedStatus(
	ctx context.Context, urlParams *url.Values, statusMap map[string]string) (
	[]string, error,
) {
	pools, err := timeseries.PoolsWithDeposit(ctx)
	if err != nil {
		return nil, err
	}
	requestedStatus := util.ConsumeUrlParam(urlParams, "status")
	if requestedStatus != "" {
		const errormsg = "Max one status parameter, accepted values: available, staged, suspended"
		requestedStatus = strings.ToLower(requestedStatus)
		// Allowed statuses in
		// https://gitlab.com/thorchain/thornode/-/blob/master/x/thorchain/types/type_pool.go
		if requestedStatus != "available" && requestedStatus != "staged" && requestedStatus != "suspended" {
			return nil, fmt.Errorf(errormsg)
		}
	}
	ret := []string{}
	for _, pool := range pools {
		poolStatus := poolStatusFromMap(pool, statusMap)
		if poolStatus != "suspended" && (requestedStatus == "" || poolStatus == requestedStatus) {
			ret = append(ret, pool)
		}
	}
	return ret, nil
}

type SaverData struct {
	SaversUnits int64
	SaversDepth int64
}

type poolAggregates struct {
	depths               timeseries.DepthMap
	dailyVolumes         map[string]int64
	liquidityUnits       map[string]int64
	annualPercentageRate map[string]float64
	saverDataMap         map[string]SaverData
	lendingDataMap       map[string]timeseries.LendingInfo
}

func getPoolAggregates(ctx context.Context, pools []string, apyBucket db.Buckets) (
	*poolAggregates, error,
) {
	latestState := timeseries.Latest.GetState()
	now := latestState.NextSecond()

	var dailyVolumes map[string]int64
	if poolVol24job != nil && poolVol24job.response.buf.Len() > 0 {
		err := json.Unmarshal(poolVol24job.response.buf.Bytes(), &dailyVolumes)
		if err != nil {
			return nil, err
		}
	} else {
		window24h := db.Window{From: now - 24*60*60, Until: now}
		var err error
		dailyVolumes, err = stat.PoolsTotalVolume(ctx, pools, window24h)
		if err != nil {
			return nil, err
		}
	}

	// this adds pool synth for pool endpoint
	if len(pools) == 1 {
		synthPool := strings.Replace(pools[0], ".", "/", 1)
		_, ok := latestState.Pools[synthPool]
		if ok {
			pools = append(pools, synthPool)
		}
	}

	liquidityUnitsNow, err := stat.PoolsLiquidityUnitsBefore(ctx, pools, nil)
	if err != nil {
		return nil, err
	}

	saverData := getSaversData(latestState.Pools, liquidityUnitsNow)

	// Todo: Use Job
	aprs, err := GetPoolAPRs(ctx, latestState.Pools, liquidityUnitsNow, pools,
		apyBucket.Start().ToNano(), apyBucket.End().ToNano())
	if err != nil {
		return nil, err
	}

	mapLendingInfo, err := timeseries.GetLendingData(ctx)
	if err != nil {
		return nil, err
	}
	aggregates := poolAggregates{
		depths:               latestState.Pools,
		dailyVolumes:         dailyVolumes,
		liquidityUnits:       liquidityUnitsNow,
		annualPercentageRate: aprs,
		saverDataMap:         saverData,
		lendingDataMap:       mapLendingInfo,
	}

	return &aggregates, nil
}

func getSaversData(depths timeseries.DepthMap, liquidityUnits map[string]int64) map[string]SaverData {
	ret := map[string]SaverData{}
	for p := range depths {
		actualPoolName := strings.Replace(p, "/", ".", 1)
		if record.GetCoinType([]byte(p)) == record.AssetSynth {
			ret[actualPoolName] = SaverData{
				SaversDepth: depths[p].AssetDepth,
				SaversUnits: liquidityUnits[p],
			}
		}
	}
	return ret
}

func poolStatusFromMap(pool string, statusMap map[string]string) string {
	status, ok := statusMap[pool]
	if !ok {
		status = timeseries.DefaultPoolStatus
	}
	return status
}

func buildPoolDetail(
	ctx context.Context, pool, status string, aggregates poolAggregates, runePriceUsd float64, decimal int64,
) oapigen.PoolDetail {
	assetDepth := aggregates.depths[pool].AssetDepth
	runeDepth := aggregates.depths[pool].RuneDepth
	synthSupply := aggregates.depths[pool].SynthDepth
	dailyVolume := aggregates.dailyVolumes[pool]
	liquidityUnits := aggregates.liquidityUnits[pool]
	synthUnits := timeseries.CalculateSynthUnits(assetDepth, synthSupply, liquidityUnits)
	poolUnits := liquidityUnits + synthUnits
	apr := aggregates.annualPercentageRate[pool]
	price := timeseries.AssetPrice(assetDepth, runeDepth)
	priceUSD := price * runePriceUsd
	saversUnit := aggregates.saverDataMap[pool].SaversUnits
	saversDepth := aggregates.saverDataMap[pool].SaversDepth
	totalCollateral := aggregates.lendingDataMap[pool].TotalCollateral
	totalDebtTor := aggregates.lendingDataMap[pool].TotalDebtTor

	saversAPR := 0.00
	if _, ok := aggregates.depths[util.ConvertNativePoolToSynth(pool)]; ok {
		saversAPR = aggregates.annualPercentageRate[util.ConvertNativePoolToSynth(pool)]
	}

	return oapigen.PoolDetail{
		Asset:                pool,
		AssetDepth:           util.IntStr(assetDepth),
		RuneDepth:            util.IntStr(runeDepth),
		AnnualPercentageRate: floatStr(apr),
		PoolAPY:              floatStr(util.Max(apr, 0)),
		AssetPrice:           floatStr(price),
		AssetPriceUSD:        floatStr(priceUSD),
		Status:               status,
		Units:                util.IntStr(poolUnits),
		LiquidityUnits:       util.IntStr(liquidityUnits),
		SynthUnits:           util.IntStr(synthUnits),
		SynthSupply:          util.IntStr(synthSupply),
		Volume24h:            util.IntStr(dailyVolume),
		NativeDecimal:        util.IntStr(decimal),
		SaversUnits:          util.IntStr(saversUnit),
		SaversDepth:          util.IntStr(saversDepth),
		SaversAPR:            floatStr(saversAPR),
		TotalCollateral:      util.IntStr(totalCollateral),
		TotalDebtTor:         util.IntStr(totalDebtTor),
	}
}

func jsonPools(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	f := func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		urlParams := r.URL.Query()

		_, lastTime, _ := timeseries.LastBlock()
		statusMap, err := timeseries.GetPoolsStatuses(r.Context(), db.Nano(lastTime.UnixNano()))
		if err != nil {
			respError(w, err)
			return
		}
		pools, err := poolsWithRequestedStatus(r.Context(), &urlParams, statusMap)
		if err != nil {
			respError(w, err)
			return
		}

		apyBucket, err := parsePeriodParam(&urlParams)
		if err != nil {
			miderr.BadRequest(err.Error()).ReportHTTP(w)
			return
		}

		merr := util.CheckUrlEmpty(urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		aggregates, err := getPoolAggregates(r.Context(), pools, apyBucket)
		if err != nil {
			respError(w, err)
			return
		}

		runePriceUsd := stat.RunePriceUSD()

		poolsDecimal := decimal.PoolsDecimal()

		poolsResponse := oapigen.PoolsResponse{}
		for _, pool := range pools {
			runeDepth := aggregates.depths[pool].RuneDepth
			assetDepth := aggregates.depths[pool].AssetDepth
			poolDecimal, ok := poolsDecimal[pool]
			if !ok {
				poolDecimal.NativeDecimals = -1
			}
			if 0 < runeDepth && 0 < assetDepth {
				status := poolStatusFromMap(pool, statusMap)
				poolsResponse = append(poolsResponse, buildPoolDetail(r.Context(), pool, status, *aggregates, runePriceUsd, poolDecimal.NativeDecimals))
			}
		}

		respJSON(w, poolsResponse)
	}
	GlobalApiCacheStore.Get(GlobalApiCacheStore.MidTermLifetime, f, w, r, params)
}

func jsonohlcv(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	f := func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		pool := ps[0].Value

		if !timeseries.PoolExists(pool) {
			miderr.BadRequestF("Unknown pool: %s", pool).ReportHTTP(w)
			return
		}

		urlParams := r.URL.Query()
		buckets, merr := db.BucketsFromQuery(r.Context(), &urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		merr = util.CheckUrlEmpty(urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		depths, err := stat.PoolOHLCVHistory(r.Context(), buckets, pool)
		if err != nil {
			miderr.InternalErrE(err).ReportHTTP(w)
			return
		}
		swaps, err := stat.GetPoolSwaps(r.Context(), &pool, buckets, false, false)
		if err != nil {
			miderr.InternalErrE(err).ReportHTTP(w)
			return
		}
		for i := 0; i < buckets.Count(); i++ {
			depths[i].Depths.Volume = swaps[i].TotalVolume
		}
		var result oapigen.OHLCVHistoryResponse = toOapiOhlcvResponse(depths)
		respJSON(w, result)
	}
	GlobalApiCacheStore.Get(GlobalApiCacheStore.LongTermLifetime, f, w, r, ps)
}

func jsonPool(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	f := func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		urlParams := r.URL.Query()

		apyBucket, err := parsePeriodParam(&urlParams)
		if err != nil {
			miderr.BadRequest(err.Error()).ReportHTTP(w)
			return
		}

		merr := util.CheckUrlEmpty(urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		pool := ps[0].Value

		if !timeseries.PoolExistsNow(pool) {
			miderr.BadRequestF("Unknown pool: %s", pool).ReportHTTP(w)
			return
		}

		status, err := timeseries.PoolStatus(r.Context(), pool)
		if err != nil {
			miderr.InternalErrE(err).ReportHTTP(w)
			return
		}

		aggregates, err := getPoolAggregates(r.Context(), []string{pool}, apyBucket)
		if err != nil {
			miderr.InternalErrE(err).ReportHTTP(w)
			return
		}

		runePriceUsd := stat.RunePriceUSD()

		poolsDecimal := decimal.PoolsDecimal()

		poolDecimal, ok := poolsDecimal[pool]
		if !ok {
			poolDecimal.NativeDecimals = -1
		}

		poolResponse := oapigen.PoolResponse(
			buildPoolDetail(r.Context(), pool, status, *aggregates, runePriceUsd, poolDecimal.NativeDecimals))
		respJSON(w, poolResponse)
	}
	GlobalApiCacheStore.Get(GlobalApiCacheStore.ShortTermLifetime, f, w, r, ps)
}

func parsePeriodParam(urlParams *url.Values) (db.Buckets, error) {
	period := util.ConsumeUrlParam(urlParams, "period")
	if period == "" {
		period = "14d"
	}
	var buckets db.Buckets
	now := db.NowSecond()
	switch period {
	case "1h":
		buckets = db.Buckets{Timestamps: db.Seconds{now - 60*60, now}}
	case "24h":
		buckets = db.Buckets{Timestamps: db.Seconds{now - 24*60*60, now}}
	case "7d":
		buckets = db.Buckets{Timestamps: db.Seconds{now - 7*24*60*60, now}}
	case "14d":
		buckets = db.Buckets{Timestamps: db.Seconds{now - 14*24*60*60, now}}
	case "30d":
		buckets = db.Buckets{Timestamps: db.Seconds{now - 30*24*60*60, now}}
	case "90d":
		buckets = db.Buckets{Timestamps: db.Seconds{now - 90*24*60*60, now}}
	case "100d":
		buckets = db.Buckets{Timestamps: db.Seconds{now - 100*24*60*60, now}}
	case "180d":
		buckets = db.Buckets{Timestamps: db.Seconds{now - 180*24*60*60, now}}
	case "365d":
		buckets = db.Buckets{Timestamps: db.Seconds{now - 365*24*60*60, now}}
	case "all":
		buckets = db.AllHistoryBuckets()
	default:
		return db.Buckets{}, fmt.Errorf(
			"invalid `period` param: %s. Accepted values:  1h, 24h, 7d, 14d, 30d, 90d, 100d, 180d, 365d, all",
			period)
	}

	return buckets, nil
}

// returns string array
func jsonMembers(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	urlParams := r.URL.Query()

	var pool *string
	poolParam := util.ConsumeUrlParam(&urlParams, "pool")
	if poolParam != "" {
		pool = &poolParam
		if !timeseries.PoolExists(*pool) {
			miderr.BadRequestF("Unknown pool: %s", *pool).ReportHTTP(w)
			return
		}

	}
	merr := util.CheckUrlEmpty(urlParams)
	if merr != nil {
		merr.ReportHTTP(w)
		return
	}

	addrs, err := timeseries.GetMemberIds(r.Context(), pool)
	if err != nil {
		respError(w, err)
		return
	}
	result := oapigen.MembersResponse(addrs)
	respJSON(w, result)
}

func jsonMemberDetails(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	urlParams := r.URL.Query()

	showPoolsType := timeseries.RegularPools
	if util.ConsumeUrlParam(&urlParams, "showSavers") == "true" {
		showPoolsType = timeseries.RegularAndSaverPools
	}

	if merr := util.CheckUrlEmpty(urlParams); merr != nil {
		merr.ReportHTTP(w)
		return
	}

	addr := strings.Join(withLowered(ps[0].Value), ",")

	addrs := strings.Split(addr, ",")
	pools, err := timeseries.GetMemberPools(r.Context(), addrs, showPoolsType)
	if err != nil {
		respError(w, err)
		return
	}
	pools, err = timeseries.CheckPools(pools)
	if err != nil {
		http.Error(w, "Failed to fetch pools", http.StatusInternalServerError)
		return
	}

	for i := 0; i < len(pools); i++ {
		if pools[i].LiquidityUnits == 0 && pools[i].RunePending == 0 && pools[i].AssetPending == 0 {
			pools = append(pools[:i], pools[i+1:]...)
			i--
		}
	}

	if len(pools) == 0 {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}

	respJSON(w, oapigen.MemberDetailsResponse{
		Pools: pools.ToOapigen(),
	})
}

func jsonBorrowers(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	urlParams := r.URL.Query()

	var asset *string
	assetParam := util.ConsumeUrlParam(&urlParams, "asset")
	if assetParam != "" {
		asset = &assetParam
		if !timeseries.PoolExists(*asset) {
			miderr.BadRequestF("Unknown asset: %s", *asset).ReportHTTP(w)
			return
		}
	}
	merr := util.CheckUrlEmpty(urlParams)
	if merr != nil {
		merr.ReportHTTP(w)
		return
	}

	addrs, err := timeseries.GetBorrowerIds(r.Context(), asset)
	if err != nil {
		respError(w, err)
		return
	}
	result := oapigen.Borrowers(addrs)
	respJSON(w, result)
}

func jsonBorrowerDetails(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	urlParams := r.URL.Query()

	if merr := util.CheckUrlEmpty(urlParams); merr != nil {
		merr.ReportHTTP(w)
		return
	}

	addr := strings.Join(withLowered(ps[0].Value), ",")

	addrs := strings.Split(addr, ",")
	pools, err := timeseries.GetBorrower(r.Context(), addrs)
	if err != nil {
		respError(w, err)
		return
	}

	if len(pools) == 0 {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}

	respJSON(w, oapigen.BorrowerDetailsResponse{
		Pools: pools.ToOapigen(),
	})
}

func getSaversRedeemValue(pools timeseries.MemberPools, poolsDepthMap timeseries.DepthMap,
	poolsUnitsMap map[string]int64,
) map[string]int64 {
	poolRedeemValueMap := map[string]int64{}
	for _, pool := range pools {
		poolUnits := float64(poolsUnitsMap[pool.Pool])
		if poolUnits == 0.0 {
			continue
		}
		saversDepth := float64(poolsDepthMap[pool.Pool].AssetDepth)
		poolRedeemValueMap[pool.Pool] = int64((saversDepth * float64(pool.LiquidityUnits)) / poolUnits)
	}

	return poolRedeemValueMap
}

func jsonSaverDetails(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	urlParams := r.URL.Query()

	if merr := util.CheckUrlEmpty(urlParams); merr != nil {
		merr.ReportHTTP(w)
		return
	}

	addr := strings.Join(withLowered(ps[0].Value), ",")

	addrs := strings.Split(addr, ",")
	pools, err := timeseries.GetMemberPools(r.Context(), addrs, timeseries.SaverPools)
	if err != nil {
		respError(w, err)
		return
	}

	if len(pools) == 0 {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}
	poolDepthMap := timeseries.Latest.GetState().Pools
	memberPools := make([]string, 0)
	for _, memberPool := range pools {
		if timeseries.PoolExists(memberPool.Pool) {
			memberPools = append(memberPools, memberPool.Pool)
		}
	}
	poolUnitsMap, err := stat.CurrentPoolsLiquidityUnits(r.Context(), memberPools)
	if err != nil {
		respError(w, err)
		return
	}

	poolRedeemValue := getSaversRedeemValue(pools, poolDepthMap, poolUnitsMap)

	respJSON(w, oapigen.SaverDetailsResponse{
		Pools: pools.ToSavers(poolRedeemValue),
	})
}

func jsonFullMemberDetails(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	f := func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		urlParams := r.URL.Query()
		addrs := util.ConsumeUrlParam(&urlParams, "address")
		address := strings.Split(addrs, ",")
		var allPools []oapigen.FullMemberPool
		for _, addr := range address {
			var pools timeseries.MemberPools
			var err error
			for _, addr := range []string{addr, strings.ToLower(addr)} {
				AddLog(fmt.Sprintf("Try to GetFullMemberPools for %s (%d)", addr, time.Now().Unix()))
				pools, err = timeseries.GetFullMemberPools(r.Context(), addr)
				AddLog(fmt.Sprintf("End of GetFullMemberPools for %s (%d)", addr, time.Now().Unix()))
				if err != nil {
					respError(w, err)
					return
				}
				if len(pools) > 0 {
					break
				}
			}
			AddLog(fmt.Sprintf("Try to CheckPools for %s (%d)", addr, time.Now().Unix()))
			pools, err = timeseries.CheckPools(pools)
			AddLog(fmt.Sprintf("End of CheckPools for %s (%d)", addr, time.Now().Unix()))
			AddLog(config.Global.ThorChain.ThorNodeURL)
			AddLog(fmt.Sprintf("Fetch pools for %s (%d)", err, time.Now().Unix()))
			if err != nil {
				http.Error(w, "Failed to fetch pools", http.StatusInternalServerError)
				return
			}

			poolNames := make([]string, 0)
			for i := 0; i < len(pools); i++ {
				if pools[i].LiquidityUnits == 0 && pools[i].RunePending == 0 && pools[i].AssetPending == 0 {
					pools = append(pools[:i], pools[i+1:]...)
					i--
				} else {
					poolNames = append(poolNames, pools[i].Pool)
				}
			}

			AddLog(fmt.Sprintf("Try to get pool stats for %s (%d)", addr, time.Now().Unix()))
			liquidityUnits := getCurrentUnit()
			AddLog(fmt.Sprintf("End of get pool stats for %s (%d)", addr, time.Now().Unix()))
			for _, memberPool := range pools {
				AddLog(fmt.Sprintf("Try to get latest stats for %s (%d)", addr, time.Now().Unix()))
				state := timeseries.Latest.GetState()
				AddLog(fmt.Sprintf("End of get latest stats for %s (%d)", addr, time.Now().Unix()))
				AddLog(fmt.Sprintf("Try to PoolInfo for %s (%d)", addr, time.Now().Unix()))
				poolInfo := state.PoolInfo(memberPool.Pool)
				AddLog(fmt.Sprintf("End of PoolInfo for %s (%d)", addr, time.Now().Unix()))

				var synthUnits int64
				var PoolUnits int64
				if poolInfo == nil {
					synthUnits = 0
					PoolUnits = 0
				} else {
					synthUnits = timeseries.CalculateSynthUnits(poolInfo.AssetDepth, poolInfo.SynthDepth, liquidityUnits[memberPool.Pool])
					PoolUnits = liquidityUnits[memberPool.Pool] + synthUnits
				}
				latestState := timeseries.Latest.GetState()
				var poolAssetDepth int64
				var poolRuneDepth int64
				if _, ok := latestState.Pools[memberPool.Pool]; ok {
					poolAssetDepth = latestState.Pools[memberPool.Pool].AssetDepth
					poolRuneDepth = latestState.Pools[memberPool.Pool].RuneDepth
				}

				allPools = append(allPools, oapigen.FullMemberPool{
					Pool:           memberPool.Pool,
					RuneAddress:    memberPool.RuneAddress,
					AssetAddress:   memberPool.AssetAddress,
					PoolUnits:      util.IntStr(PoolUnits),
					SharedUnits:    util.IntStr(memberPool.LiquidityUnits),
					RuneAdded:      util.IntStr(memberPool.RuneAdded),
					AssetAdded:     util.IntStr(memberPool.AssetAdded),
					RuneWithdrawn:  util.IntStr(memberPool.RuneWithdrawn),
					AssetWithdrawn: util.IntStr(memberPool.AssetWithdrawn),
					RunePending:    util.IntStr(memberPool.RunePending),
					AssetPending:   util.IntStr(memberPool.AssetPending),
					DateFirstAdded: util.IntStr(memberPool.DateFirstAdded),
					DateLastAdded:  util.IntStr(memberPool.DateLastAdded),
					PoolAssetDepth: util.IntStr(poolAssetDepth),
					PoolRuneDepth:  util.IntStr(poolRuneDepth),
				})
			}
		}
		for i := 0; i < len(allPools); i++ {
			for j := i + 1; j < len(allPools); j++ {
				if reflect.DeepEqual(allPools[i], allPools[j]) {
					allPools = append(allPools[:j], allPools[j+1:]...)
					j--
				}
			}
		}
		respJSON(w, allPools)
	}
	GlobalApiCacheStore.Get(GlobalApiCacheStore.MidTermLifetime, f, w, r, params)
}

/*func jsonLPDetailsV2(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	f := func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		addr := ps[0].Value

		urlParams := r.URL.Query()
		poolsStr := util.ConsumeUrlParam(&urlParams, "pools")
		if poolsStr == "" {
			http.Error(w, "Invalid pools", http.StatusBadRequest)
			return
		}
		pools := strings.Split(poolsStr, ",")
		AddLog(fmt.Sprintf("Try to GetFullMemberPools for %s (%d)", addr, time.Now().Unix()))
		allPools, err := timeseries.GetFullMemberPools(r.Context(), addr)
		AddLog(fmt.Sprintf("End of GetFullMemberPools for %s (%d)", addr, time.Now().Unix()))
		if err != nil {
			respError(w, err)
			return
		}
		for i := 0; i < len(allPools); i++ {
			selected := false
			for _, pool := range pools {
				if allPools[i].Pool == pool {
					selected = true
				}
			}
			if !selected {
				allPools = append(allPools[:i], allPools[i+1:]...)
				i--
			}
		}
		AddLog(fmt.Sprintf("Try to CeckPools for %s (%d)", addr, time.Now().Unix()))
		allPools, err = timeseries.CheckPools(allPools)
		if err != nil {
			http.Error(w, "Failed to fetch pools", http.StatusInternalServerError)
			return
		}
		AddLog(fmt.Sprintf("End of CheckPools for %s (%d)", addr, time.Now().Unix()))

		for i := 0; i < len(allPools); i++ {
			if allPools[i].LiquidityUnits == 0 && allPools[i].RunePending == 0 && allPools[i].AssetPending == 0 {
				allPools = append(allPools[:i], allPools[i+1:]...)
				i--
			}
		}

		var selectedPools []timeseries.MemberPool
		for _, memberPool := range allPools {
			for _, pool := range pools {
				if memberPool.Pool == pool {
					selectedPools = append(selectedPools, memberPool)
				}
			}
		}
		if len(selectedPools) == 0 {
			http.Error(w, "Not Found", http.StatusNotFound)
			return
		}

		blockStat := timeseries.Latest.GetState()
		AddLog(fmt.Sprintf("Try to PoolsLiquidityUnitsBefore for %s (%d)", addr, time.Now().Unix()))
		ctx1, _ := context.WithTimeout(context.Background(), time.Minute)
		liquidityUnitsNow, err := stat.PoolsLiquidityUnitsBefore(ctx1, pools, nil)
		if err != nil {
			http.Error(w, "Not Found", http.StatusNotFound)
			return
		}
		AddLog(fmt.Sprintf("End of PoolsLiquidityUnitsBefore for %s (%d)", addr, time.Now().Unix()))

		var lpDetails []oapigen.LPDetail
		for _, memberPool := range selectedPools {
			var lpDetail []timeseries.LPDetail
			AddLog(fmt.Sprintf("Try to GetLpDetail for %s (%d)", addr, time.Now().Unix()))
			lpDetail, err = timeseries.GetLpDetail(r.Context(), memberPool.RuneAddress, memberPool.AssetAddress, memberPool.Pool)
			AddLog(fmt.Sprintf("End of GetLpDetail for %s (%d)", addr, time.Now().Unix()))
			if err != nil {
				respError(w, err)
				return
			}
			for i := 0; i < len(lpDetail); i++ {
				for j := i + 1; j < len(lpDetail); j++ {
					if lpDetail[i].Date > lpDetail[j].Date {
						temp := lpDetail[i]
						lpDetail[i] = lpDetail[j]
						lpDetail[j] = temp
					}
				}
			}
			for i := 0; i < len(lpDetail); i++ {
				var runeFee, assetFee, reward float64
				if i != len(lpDetail)-1 {
					runeFee = lpDetail[i+1].RuneFee - lpDetail[i].RuneFee
					assetFee = lpDetail[i+1].AssetFee - lpDetail[i].AssetFee
					reward = lpDetail[i+1].Reward - lpDetail[i].Reward
				} else {
					runeFee = record.Recorder.GetPoolRuneFee(lpDetail[i].Pool) - lpDetail[i].RuneFee
					assetFee = record.Recorder.GetPoolAssetFee(lpDetail[i].Pool) - lpDetail[i].AssetFee
					reward = record.Recorder.GetPoolReward(lpDetail[i].Pool) - lpDetail[i].Reward
				}
				lpDetail[i].RuneLiquidityFee = int64(runeFee)
				lpDetail[i].AssetLiquidityFee = int64(assetFee)
				lpDetail[i].BlockRewards = int64(reward)
			}
			assetPrice := float64(blockStat.Pools[memberPool.Pool].RuneDepth) / float64(blockStat.Pools[memberPool.Pool].AssetDepth)
			runePrice := stat.RunePriceUSD()

			units := int64(0)
			stakeDetail := make([]oapigen.StakeDetail, 0)
			withdrawDetail := make([]oapigen.StakeDetail, 0)
			stakedAsset := int64(0)
			stakedRune := int64(0)
			stakedUsd := int64(0)
			runeFees := float64(0)
			assetFees := float64(0)
			usdFees := float64(0)
			rewards := float64(0)
			totalUsd := float64(0)
			for _, lp := range lpDetail {
				units += lp.LiquidityUnits
				totalUsd = float64(units) * float64(blockStat.Pools[memberPool.Pool].AssetDepth) * assetPrice * runePrice
				totalUsd += float64(units) * float64(blockStat.Pools[memberPool.Pool].RuneDepth) * runePrice
				if totalUsd <= 1 || units == 0 {
					units = int64(0)
					stakeDetail = make([]oapigen.StakeDetail, 0)
					withdrawDetail = make([]oapigen.StakeDetail, 0)
					stakedAsset = int64(0)
					stakedRune = int64(0)
					stakedUsd = int64(0)
					runeFees = float64(0)
					assetFees = float64(0)
					usdFees = float64(0)
					rewards = float64(0)
					totalUsd = float64(0)
					continue
				}
				runeFee := float64(lp.RuneLiquidityFee) * float64(units) / float64(lp.PoolUnit)
				assetFee := float64(lp.AssetLiquidityFee) * float64(units) / float64(lp.PoolUnit)
				usdFee := runeFee*lp.RunePriceUsd + assetFee*lp.AssetPriceUsd
				runeFees += runeFee
				assetFees += assetFee
				usdFees += usdFee
				rewards += float64(lp.BlockRewards) * float64(units) / float64(liquidityUnitsNow[memberPool.Pool])
				if lp.AssetAdded > 0 || lp.RuneAdded > 0 {
					stakeDetail = append(stakeDetail, oapigen.StakeDetail{
						AssetAmount:   util.IntStr(lp.AssetAdded),
						RuneAmount:    util.IntStr(lp.RuneAdded),
						AssetPriceUsd: floatStr(lp.AssetPriceUsd),
						RunePriceUsd:  floatStr(lp.RunePriceUsd),
						AssetPrice:    floatStr(lp.AssetPriceUsd / lp.RunePriceUsd),
						Date:          util.IntStr(lp.Date),
						Height:        util.IntStr(lp.Height),
						AssetDepth:    util.IntStr(lp.AssetDepth),
						RuneDepth:     util.IntStr(lp.RuneDepth),
						PoolUnits:     util.IntStr(lp.PoolUnit),
						SharedUnits:   util.IntStr(lp.LiquidityUnits),
					})
				} else {
					withdrawDetail = append(withdrawDetail, oapigen.StakeDetail{
						AssetAmount:   util.IntStr(lp.AssetWithdrawn),
						RuneAmount:    util.IntStr(lp.RuneWithdrawn),
						AssetPriceUsd: floatStr(lp.AssetPriceUsd),
						RunePriceUsd:  floatStr(lp.RunePriceUsd),
						AssetPrice:    floatStr(lp.AssetPriceUsd / lp.RunePriceUsd),
						Date:          util.IntStr(lp.Date),
						Height:        util.IntStr(lp.Height),
						AssetDepth:    util.IntStr(lp.AssetDepth),
						RuneDepth:     util.IntStr(lp.RuneDepth),
						PoolUnits:     util.IntStr(lp.PoolUnit),
						SharedUnits:   util.IntStr(lp.LiquidityUnits),
						BasisPoint:    floatStr(10000 * float64(-1*lp.LiquidityUnits) / float64(units-lp.LiquidityUnits)),
					})
				}
				stakedAsset = stakedAsset + lp.AssetAdded - lp.AssetWithdrawn
				stakedRune = stakedRune + lp.RuneAdded - lp.RuneWithdrawn
				stakedUsd = stakedUsd + int64(float64(lp.AssetAdded-lp.AssetWithdrawn)*lp.AssetPriceUsd)
				stakedUsd = stakedUsd + int64(float64(lp.RuneAdded-lp.RuneWithdrawn)*lp.RunePriceUsd)
			}

			lpDetails = append(lpDetails, oapigen.LPDetail{
				SharedUnits:    util.IntStr(memberPool.LiquidityUnits),
				StakeDetail:    stakeDetail,
				WithdrawDetail: withdrawDetail,
				AssetEarned:    floatStr(assetFees),
				RuneEarned:     floatStr(runeFees),
				UsdEarned:      floatStr(usdFees),
				Rewards:        floatStr(rewards),
				Pool:           memberPool.Pool,
				RuneAddress:    memberPool.RuneAddress,
				AssetAddress:   memberPool.AssetAddress,
			})
		}
		AddLog(fmt.Sprintf("Try to AllDepths for %s (%d)", addr, time.Now().Unix()))
		assetE8DepthPerPool, runeE8DepthPerPool, _, _ := timeseries.AllDepths()
		AddLog(fmt.Sprintf("End of AllDepths for %s (%d)", addr, time.Now().Unix()))
		AddLog(fmt.Sprintf("Try to CurrentPoolsLiquidityUnits for %s (%d)", addr, time.Now().Unix()))
		liquidityUnits := getCurrentUnit()
		AddLog(fmt.Sprintf("End of CurrentPoolsLiquidityUnits for %s (%d)", addr, time.Now().Unix()))
		for i, lp := range lpDetails {
			assetPrice := float64(runeE8DepthPerPool[lp.Pool]) / float64(assetE8DepthPerPool[lp.Pool])
			runePrice := stat.RunePriceUSD()
			lpDetails[i].AssetDepth = util.IntStr(assetE8DepthPerPool[lp.Pool])
			lpDetails[i].RuneDepth = util.IntStr(runeE8DepthPerPool[lp.Pool])
			lpDetails[i].AssetPriceUsd = floatStr(assetPrice * runePrice)
			lpDetails[i].AssetPrice = floatStr(assetPrice)
			lpDetails[i].RunePriceUsd = floatStr(runePrice)
			state := timeseries.Latest.GetState()
			poolInfo := state.PoolInfo(lp.Pool)
			synthUnits := timeseries.CalculateSynthUnits(poolInfo.AssetDepth, poolInfo.SynthDepth, liquidityUnits[lp.Pool])
			lpDetails[i].PoolUnits = util.IntStr(liquidityUnits[lp.Pool] + synthUnits)
		}
		respJSON(w, lpDetails)
	}
	GlobalApiCacheStore.Get(GlobalApiCacheStore.MidTermLifetime, f, w, r, params)
}*/

func jsonLPDetails(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	f := func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		addr := ps[0].Value

		urlParams := r.URL.Query()
		poolsStr := util.ConsumeUrlParam(&urlParams, "pools")
		if poolsStr == "" {
			http.Error(w, "Invalid pools", http.StatusBadRequest)
			return
		}
		pools := strings.Split(poolsStr, ",")
		AddLog(fmt.Sprintf("Try to GetFullMemberPools for %s (%d)", addr, time.Now().Unix()))
		allPools, err := timeseries.GetFullMemberPools(r.Context(), addr)
		AddLog(fmt.Sprintf("End of GetFullMemberPools for %s (%d)", addr, time.Now().Unix()))
		if err != nil {
			respError(w, err)
			return
		}
		for i := 0; i < len(allPools); i++ {
			selected := false
			for _, pool := range pools {
				if allPools[i].Pool == pool {
					selected = true
				}
			}
			if !selected {
				allPools = append(allPools[:i], allPools[i+1:]...)
				i--
			}
		}
		AddLog(fmt.Sprintf("Try to CeckPools for %s (%d)", addr, time.Now().Unix()))
		allPools, err = timeseries.CheckPools(allPools)
		if err != nil {
			http.Error(w, "Failed to fetch pools", http.StatusInternalServerError)
			return
		}
		AddLog(fmt.Sprintf("End of CheckPools for %s (%d)", addr, time.Now().Unix()))

		for i := 0; i < len(allPools); i++ {
			if allPools[i].LiquidityUnits == 0 && allPools[i].RunePending == 0 && allPools[i].AssetPending == 0 {
				allPools = append(allPools[:i], allPools[i+1:]...)
				i--
			}
		}

		var selectedPools []timeseries.MemberPool
		for _, memberPool := range allPools {
			for _, pool := range pools {
				if memberPool.Pool == pool {
					selectedPools = append(selectedPools, memberPool)
				}
			}
		}
		if len(selectedPools) == 0 {
			http.Error(w, "Not Found", http.StatusNotFound)
			return
		}

		blockStat := timeseries.Latest.GetState()
		AddLog(fmt.Sprintf("Try to PoolsLiquidityUnitsBefore for %s (%d)", addr, time.Now().Unix()))
		ctx1, _ := context.WithTimeout(context.Background(), time.Minute)
		liquidityUnitsNow, err := stat.PoolsLiquidityUnitsBefore(ctx1, pools, nil)
		if err != nil {
			http.Error(w, "Not Found", http.StatusNotFound)
			return
		}
		AddLog(fmt.Sprintf("End of PoolsLiquidityUnitsBefore for %s (%d)", addr, time.Now().Unix()))

		var lpDetails []oapigen.LPDetail
		for _, memberPool := range selectedPools {
			var lpDetail []timeseries.LPDetail
			AddLog(fmt.Sprintf("Try to GetLpDetail for %s (%d)", addr, time.Now().Unix()))
			lpDetail, err = timeseries.GetLpDetail(r.Context(), memberPool.RuneAddress, memberPool.AssetAddress, memberPool.Pool)
			AddLog(fmt.Sprintf("End of GetLpDetail for %s (%d)", addr, time.Now().Unix()))
			if err != nil {
				respError(w, err)
				return
			}
			for i := 0; i < len(lpDetail); i++ {
				for j := i + 1; j < len(lpDetail); j++ {
					if lpDetail[i].Date > lpDetail[j].Date {
						temp := lpDetail[i]
						lpDetail[i] = lpDetail[j]
						lpDetail[j] = temp
					}
				}
			}
			for i := 0; i < len(lpDetail); i++ {
				start := lpDetail[i].Date
				end := int64(-1)
				if i+1 < len(lpDetail) {
					end = lpDetail[i+1].Date
				}
				AddLog(fmt.Sprintf("Try to GetPoolSwapsFee for %s (%d)", addr, time.Now().Unix()))
				res, err := stat.GetPoolSwapsFee(r.Context(), memberPool.Pool, start, end)
				if err != nil {
					miderr.InternalErrE(err).ReportHTTP(w)
					return
				}
				AddLog(fmt.Sprintf("End of GetPoolSwapsFee for %s (%d)", addr, time.Now().Unix()))
				AddLog(fmt.Sprintf("Try to GetPoolRewards for %s (%d)", addr, time.Now().Unix()))
				rewards, err := stat.GetPoolRewards(r.Context(), memberPool.Pool, start, end)
				if err != nil {
					miderr.InternalErrE(err).ReportHTTP(w)
					return
				}
				AddLog(fmt.Sprintf("End of GetPoolRewards for %s (%d)", addr, time.Now().Unix()))
				lpDetail[i].RuneLiquidityFee = res.RuneAmount
				lpDetail[i].AssetLiquidityFee = res.AssetAmount
				lpDetail[i].BlockRewards = rewards
			}
			assetPrice := float64(blockStat.Pools[memberPool.Pool].RuneDepth) / float64(blockStat.Pools[memberPool.Pool].AssetDepth)
			runePrice := stat.RunePriceUSD()

			units := int64(0)
			stakeDetail := make([]oapigen.StakeDetail, 0)
			withdrawDetail := make([]oapigen.StakeDetail, 0)
			stakedAsset := int64(0)
			stakedRune := int64(0)
			stakedUsd := int64(0)
			runeFees := float64(0)
			assetFees := float64(0)
			usdFees := float64(0)
			rewards := float64(0)
			totalUsd := float64(0)
			for _, lp := range lpDetail {
				units += lp.LiquidityUnits
				totalUsd = float64(units) * float64(blockStat.Pools[memberPool.Pool].AssetDepth) * assetPrice * runePrice
				totalUsd += float64(units) * float64(blockStat.Pools[memberPool.Pool].RuneDepth) * runePrice
				if totalUsd <= 1 || units == 0 {
					units = int64(0)
					stakeDetail = make([]oapigen.StakeDetail, 0)
					withdrawDetail = make([]oapigen.StakeDetail, 0)
					stakedAsset = int64(0)
					stakedRune = int64(0)
					stakedUsd = int64(0)
					runeFees = float64(0)
					assetFees = float64(0)
					usdFees = float64(0)
					rewards = float64(0)
					totalUsd = float64(0)
					continue
				}
				runeFee := float64(lp.RuneLiquidityFee) * float64(units) / float64(lp.PoolUnit)
				assetFee := float64(lp.AssetLiquidityFee) * float64(units) / float64(lp.PoolUnit)
				usdFee := runeFee*lp.RunePriceUsd + assetFee*lp.AssetPriceUsd
				runeFees += runeFee
				assetFees += assetFee
				usdFees += usdFee
				rewards += float64(lp.BlockRewards) * float64(units) / float64(liquidityUnitsNow[memberPool.Pool])
				if lp.AssetAdded > 0 || lp.RuneAdded > 0 {
					stakeDetail = append(stakeDetail, oapigen.StakeDetail{
						AssetAmount:   util.IntStr(lp.AssetAdded),
						RuneAmount:    util.IntStr(lp.RuneAdded),
						AssetPriceUsd: floatStr(lp.AssetPriceUsd),
						RunePriceUsd:  floatStr(lp.RunePriceUsd),
						AssetPrice:    floatStr(lp.AssetPriceUsd / lp.RunePriceUsd),
						Date:          util.IntStr(lp.Date),
						Height:        util.IntStr(lp.Height),
						AssetDepth:    util.IntStr(lp.AssetDepth),
						RuneDepth:     util.IntStr(lp.RuneDepth),
						PoolUnits:     util.IntStr(lp.PoolUnit),
						SharedUnits:   util.IntStr(lp.LiquidityUnits),
					})
				} else {
					withdrawDetail = append(withdrawDetail, oapigen.StakeDetail{
						AssetAmount:   util.IntStr(lp.AssetWithdrawn),
						RuneAmount:    util.IntStr(lp.RuneWithdrawn),
						AssetPriceUsd: floatStr(lp.AssetPriceUsd),
						RunePriceUsd:  floatStr(lp.RunePriceUsd),
						AssetPrice:    floatStr(lp.AssetPriceUsd / lp.RunePriceUsd),
						Date:          util.IntStr(lp.Date),
						Height:        util.IntStr(lp.Height),
						AssetDepth:    util.IntStr(lp.AssetDepth),
						RuneDepth:     util.IntStr(lp.RuneDepth),
						PoolUnits:     util.IntStr(lp.PoolUnit),
						SharedUnits:   util.IntStr(lp.LiquidityUnits),
						BasisPoint:    floatStr(10000 * float64(-1*lp.LiquidityUnits) / float64(units-lp.LiquidityUnits)),
					})
				}
				stakedAsset = stakedAsset + lp.AssetAdded - lp.AssetWithdrawn
				stakedRune = stakedRune + lp.RuneAdded - lp.RuneWithdrawn
				stakedUsd = stakedUsd + int64(float64(lp.AssetAdded-lp.AssetWithdrawn)*lp.AssetPriceUsd)
				stakedUsd = stakedUsd + int64(float64(lp.RuneAdded-lp.RuneWithdrawn)*lp.RunePriceUsd)
			}

			lpDetails = append(lpDetails, oapigen.LPDetail{
				SharedUnits:    util.IntStr(memberPool.LiquidityUnits),
				StakeDetail:    stakeDetail,
				WithdrawDetail: withdrawDetail,
				AssetEarned:    floatStr(assetFees),
				RuneEarned:     floatStr(runeFees),
				UsdEarned:      floatStr(usdFees),
				Rewards:        floatStr(rewards),
				Pool:           memberPool.Pool,
				RuneAddress:    memberPool.RuneAddress,
				AssetAddress:   memberPool.AssetAddress,
			})
		}
		AddLog(fmt.Sprintf("Try to AllDepths for %s (%d)", addr, time.Now().Unix()))
		assetE8DepthPerPool, runeE8DepthPerPool, _, _ := timeseries.AllDepths()
		AddLog(fmt.Sprintf("End of AllDepths for %s (%d)", addr, time.Now().Unix()))
		AddLog(fmt.Sprintf("Try to CurrentPoolsLiquidityUnits for %s (%d)", addr, time.Now().Unix()))
		liquidityUnits := getCurrentUnit()
		AddLog(fmt.Sprintf("End of CurrentPoolsLiquidityUnits for %s (%d)", addr, time.Now().Unix()))
		for i, lp := range lpDetails {
			assetPrice := float64(runeE8DepthPerPool[lp.Pool]) / float64(assetE8DepthPerPool[lp.Pool])
			runePrice := stat.RunePriceUSD()
			lpDetails[i].AssetDepth = util.IntStr(assetE8DepthPerPool[lp.Pool])
			lpDetails[i].RuneDepth = util.IntStr(runeE8DepthPerPool[lp.Pool])
			lpDetails[i].AssetPriceUsd = floatStr(assetPrice * runePrice)
			lpDetails[i].AssetPrice = floatStr(assetPrice)
			lpDetails[i].RunePriceUsd = floatStr(runePrice)
			state := timeseries.Latest.GetState()
			poolInfo := state.PoolInfo(lp.Pool)
			synthUnits := timeseries.CalculateSynthUnits(poolInfo.AssetDepth, poolInfo.SynthDepth, liquidityUnits[lp.Pool])
			lpDetails[i].PoolUnits = util.IntStr(liquidityUnits[lp.Pool] + synthUnits)
		}
		respJSON(w, lpDetails)
	}
	GlobalApiCacheStore.Get(GlobalApiCacheStore.MidTermLifetime, f, w, r, params)
}

func jsonTHORName(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	merr := util.CheckUrlEmpty(r.URL.Query())
	if merr != nil {
		merr.ReportHTTP(w)
		return
	}

	name := ps[0].Value

	n, err := timeseries.GetTHORName(r.Context(), name)
	if err != nil {
		respError(w, err)
		return
	}
	if n.Owner == "" {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}

	entries := make([]oapigen.THORNameEntry, len(n.Entries))
	for i, e := range n.Entries {
		entries[i] = oapigen.THORNameEntry{
			Chain:   e.Chain,
			Address: e.Address,
		}
	}

	respJSON(w, oapigen.THORNameDetailsResponse{
		Owner:   n.Owner,
		Expire:  util.IntStr(n.Expire),
		Entries: entries,
	})
}

type ThornameReverseLookupFunc func(ctx context.Context, addr string) (names []string, err error)

func jsonTHORNameReverse(
	w http.ResponseWriter, r *http.Request, ps httprouter.Params,
	lookupFunc ThornameReverseLookupFunc,
) {
	merr := util.CheckUrlEmpty(r.URL.Query())
	if merr != nil {
		merr.ReportHTTP(w)
		return
	}

	addr := ps[0].Value

	var names []string
	for _, addr := range withLowered(addr) {
		var err error
		names, err = lookupFunc(r.Context(), addr)
		if err != nil {
			respError(w, err)
			return
		}
		if 0 < len(names) {
			break
		}
	}
	if len(names) == 0 {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}

	respJSON(w, oapigen.ReverseTHORNameResponse(
		names,
	))
}

func jsonTHORNameAddress(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	jsonTHORNameReverse(w, r, ps, timeseries.GetTHORNamesByAddress)
}

func jsonTHORNameOwner(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	jsonTHORNameReverse(w, r, ps, timeseries.GetTHORNamesByOwnerAddress)
}

func calculatePoolVolume(ctx context.Context, w io.Writer) error {
	if timeseries.Latest.GetState().Timestamp == 0 {
		return errors.New("Last block is not ready yet")
	}
	pools, err := timeseries.PoolsWithDeposit(ctx)
	if err != nil {
		return err
	}
	now := db.NowSecond()
	window24h := db.Window{From: now - 24*60*60, Until: now}
	dailyVolumes, err := stat.PoolsTotalVolume(ctx, pools, window24h)
	if err != nil {
		return err
	}
	bt, err := json.Marshal(dailyVolumes)
	if err != nil {
		return err
	}
	_, err = w.Write(bt)
	return err
}

func calculatePoolLiquidityChanges(ctx context.Context, w io.Writer) error {
	if timeseries.Latest.GetState().Timestamp == 0 {
		return errors.New("Last block is not ready yet")
	}
	var urls url.Values
	urls = map[string][]string{
		"interval": {"day"},
		"count":    {"100"},
	}
	buckets, merr := db.BucketsFromQuery(ctx, &urls)
	if merr != nil {
		return merr
	}
	pool := "*"
	var res oapigen.LiquidityHistoryResponse
	res, err := stat.GetLiquidityHistory(ctx, buckets, pool)
	if err != nil {
		return err
	}
	if buckets.OneInterval() {
		res.Intervals = oapigen.LiquidityHistoryIntervals{}
	}
	bt, err := json.Marshal(res)
	if err == nil {
		_, err = w.Write(bt)
	}
	return err
}

/*func calculateOHLCV(ctx context.Context, w io.Writer) error {
	pools, err := timeseries.PoolsWithDeposit(ctx)
	if err != nil {
		return err
	}
	go func(ctx context.Context) {
		for _, pool := range pools {
			params := httprouter.Params{
				{
					"pool",
					pool,
				},
			}
			ct, _ := context.WithTimeout(context.Background(), time.Minute*5)
			req, err := http.NewRequestWithContext(ct, "GET", "http://127.0.0.1:8080/v2/history/ohlcv/"+pool+"?interval=hour&count="+strconv.Itoa(ohlcvCount), nil)
			if err != nil {
				log.Error().Interface("error", err).Str("path", req.URL.Path).Msg("panic ohlcv cron job")
			}
			writer := httptest.NewRecorder()
			jsonohlcv(writer, req, params)

			req, err = http.NewRequestWithContext(ct, "GET", "http://127.0.0.1:8080/v2/history/ohlcv/"+pool+"?interval=day&count="+strconv.Itoa(ohlcvCount), nil)
			if err != nil {
				log.Error().Interface("error", err).Str("path", req.URL.Path).Msg("panic ohlcv cron job")
			}
			writer = httptest.NewRecorder()
			jsonohlcv(writer, req, params)

			req, err = http.NewRequestWithContext(ct, "GET", "http://127.0.0.1:8080/v2/history/ohlcv/"+pool+"?interval=month&count="+strconv.Itoa(ohlcvCount), nil)
			if err != nil {
				log.Error().Interface("error", err).Str("path", req.URL.Path).Msg("panic ohlcv cron job")
			}
			writer = httptest.NewRecorder()
			jsonohlcv(writer, req, params)
		}
	}(ctx)
	return err
}*/

func jsonChurns(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	churns, err := timeseries.GetChurnsData(r.Context())
	if err != nil {
		return
	}
	respJSON(w, churns)
}

type SwapperStats struct {
	uniqueSwapperCount int64
	dailyActiveUsers   int64
	monthlyActiveUsers int64
	lastCalc           time.Time
}

var swapperStats SwapperStats

// TODO(muninn): remove cache once it's <0.5s
func calculateJsonStats(ctx context.Context, w io.Writer) error {
	state := timeseries.Latest.GetState()
	now := db.NowSecond()
	window := db.Window{From: 0, Until: now}

	// TODO(huginn): Rewrite to member table if doable, stakes/unstakes lookup is ~0.8 s
	stakes, err := stat.StakesLookup(ctx, window)
	if err != nil {
		return err
	}
	withdraws, err := stat.WithdrawsLookup(ctx, window)
	if err != nil {
		return err
	}

	swapsAll, err := stat.GlobalSwapStats(ctx, "day", 0)
	if err != nil {
		return err
	}

	swaps24h, err := stat.GlobalSwapStats(ctx, "5min", now-24*60*60)
	if err != nil {
		return err
	}

	swaps30d, err := stat.GlobalSwapStats(ctx, "hour", now-30*24*60*60)
	if err != nil {
		return err
	}

	var runeDepth int64
	for poolName, poolInfo := range state.Pools {
		if record.GetCoinType([]byte(poolName)) != record.AssetDerived {
			runeDepth += poolInfo.RuneDepth
		}
	}

	switchedRune, err := stat.SwitchedRune(ctx)
	if err != nil {
		return err
	}

	if swapperStats.lastCalc.Before(time.Now().Add(-60 * time.Minute)) {
		midlog.Info("calculating swapper stats")
		window = db.Window{From: 0, Until: now}
		uniqueSwapperCount, err := stat.GetUniqueSwapperCount(ctx, window)
		if err != nil {
			return err
		}
		window = db.Window{From: now.Add(-24 * time.Hour), Until: now}
		dailyActiveUsers, err := stat.GetUniqueSwapperCount(ctx, window)
		if err != nil {
			return err
		}
		window = db.Window{From: now.Add(-24 * 30 * time.Hour), Until: now}
		monthlyActiveUsers, err := stat.GetUniqueSwapperCount(ctx, window)
		if err != nil {
			return err
		}
		swapperStats = SwapperStats{
			uniqueSwapperCount: uniqueSwapperCount,
			dailyActiveUsers:   dailyActiveUsers,
			monthlyActiveUsers: monthlyActiveUsers,
			lastCalc:           time.Now(),
		}
	}

	runePrice := stat.RunePriceUSD()

	writeJSON(w, oapigen.StatsResponse{
		RuneDepth:                     util.IntStr(runeDepth),
		SwitchedRune:                  util.IntStr(switchedRune),
		RunePriceUSD:                  floatStr(runePrice),
		SwapVolume:                    util.IntStr(swapsAll.Totals().Volume),
		SwapCount24h:                  util.IntStr(swaps24h.Totals().Count),
		SwapCount30d:                  util.IntStr(swaps30d.Totals().Count),
		SwapCount:                     util.IntStr(swapsAll.Totals().Count),
		ToAssetCount:                  util.IntStr(swapsAll[db.RuneToAsset].Count),
		ToRuneCount:                   util.IntStr(swapsAll[db.AssetToRune].Count),
		SynthMintCount:                util.IntStr(swapsAll[db.RuneToSynth].Count),
		SynthBurnCount:                util.IntStr(swapsAll[db.SynthToRune].Count),
		DailyActiveUsers:              util.IntStr(swapperStats.dailyActiveUsers),
		MonthlyActiveUsers:            util.IntStr(swapperStats.monthlyActiveUsers),
		UniqueSwapperCount:            util.IntStr(swapperStats.uniqueSwapperCount),
		AddLiquidityVolume:            util.IntStr(stakes.TotalVolume),
		WithdrawVolume:                util.IntStr(withdraws.TotalVolume),
		ImpermanentLossProtectionPaid: util.IntStr(withdraws.ImpermanentLossProtection),
		AddLiquidityCount:             util.IntStr(stakes.Count),
		WithdrawCount:                 util.IntStr(withdraws.Count),
	})
	return nil
}

var (
	poolVol24job            *cache
	poolLiquidityChangesJob *cache
	statsJob                *cache
	// poolOHLCVJob            *cache
)

func init() {
	poolVol24job = CreateAndRegisterCache(calculatePoolVolume, "volume24")
	poolLiquidityChangesJob = CreateAndRegisterCache(calculatePoolLiquidityChanges, "poolLiqduityChanges")
	statsJob = CreateAndRegisterCache(calculateJsonStats, "stats")
	// poolOHLCVJob = CreateAndRegisterCache(calculateOHLCV, "poolOHLCV")
}

func jsonActions(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	f := func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		urlParams := r.URL.Query()
		params := timeseries.ActionsParams{
			Limit:         util.ConsumeUrlParam(&urlParams, "limit"),
			NextPageToken: util.ConsumeUrlParam(&urlParams, "nextPageToken"),
			PrevPageToken: util.ConsumeUrlParam(&urlParams, "prevPageToken"),
			Timestamp:     util.ConsumeUrlParam(&urlParams, "timestamp"),
			Height:        util.ConsumeUrlParam(&urlParams, "height"),
			FromTimestamp: util.ConsumeUrlParam(&urlParams, "fromTimestamp"),
			FromHeight:    util.ConsumeUrlParam(&urlParams, "fromHeight"),
			Offset:        util.ConsumeUrlParam(&urlParams, "offset"),
			ActionType:    util.ConsumeUrlParam(&urlParams, "type"),
			Address:       util.ConsumeUrlParam(&urlParams, "address"),
			TXId:          util.ConsumeUrlParam(&urlParams, "txid"),
			Asset:         util.ConsumeUrlParam(&urlParams, "asset"),
			Affiliate:     util.ConsumeUrlParam(&urlParams, "affiliate"),
			TXIds:         util.ConsumeUrlParam(&urlParams, "txids"),
		}

		merr := util.CheckUrlEmpty(urlParams)
		if merr != nil {
			merr.ReportHTTP(w)
			return
		}

		txids := strings.Split(params.TXId, ",")
		if len(txids) == 0 {
			txids = append(txids, "")
		}
		filteredAddresses := config.Global.FilteredAddresses
		var resp oapigen.ActionsResponse
		for _, txid := range txids {
			for _, addr := range withLowered(params.Address) {
				params.Address = addr
				if name, ok := filteredAddresses[addr]; ok {
					errMsg := "The requested address is filtered, It might be one of module addresses."
					if len(name) > 0 {
						errMsg += "\n\nLabel: %s"
						respError(w, miderr.BadRequestF(errMsg, name))
					} else {
						respError(w, miderr.BadRequestF(errMsg))
					}
					return
				}
				if txid != "" {
					params.TXId = txid
				}
				actions, err := timeseries.GetActions(r.Context(), time.Time{}, params)
				if err != nil {
					respError(w, err)
					return
				}
				if len(actions.Actions) != 0 && len(txids) == 1 {
					resp = actions
					break
				} else {
					resp.Actions = append(resp.Actions, actions.Actions...)
					resp.Meta = actions.Meta
					resp.Count = actions.Count
				}
			}
		}

		respJSON(w, resp)
	}
	urlParams := r.URL.Query()
	actionParams := timeseries.ActionsParams{
		Limit:         util.ConsumeUrlParam(&urlParams, "limit"),
		NextPageToken: util.ConsumeUrlParam(&urlParams, "nextPageToken"),
		PrevPageToken: util.ConsumeUrlParam(&urlParams, "prevPageToken"),
		Timestamp:     util.ConsumeUrlParam(&urlParams, "timestamp"),
		Height:        util.ConsumeUrlParam(&urlParams, "height"),
		FromTimestamp: util.ConsumeUrlParam(&urlParams, "fromTimestamp"),
		FromHeight:    util.ConsumeUrlParam(&urlParams, "fromHeight"),
		Offset:        util.ConsumeUrlParam(&urlParams, "offset"),
		ActionType:    util.ConsumeUrlParam(&urlParams, "type"),
		Address:       util.ConsumeUrlParam(&urlParams, "address"),
		TXId:          util.ConsumeUrlParam(&urlParams, "txid"),
		Asset:         util.ConsumeUrlParam(&urlParams, "asset"),
		//	AssetType:     util.ConsumeUrlParam(&urlParams, "assetType"),
		Affiliate: util.ConsumeUrlParam(&urlParams, "affiliate"),
	}
	if actionParams.TXId == "" && actionParams.Address == "" {
		GlobalApiCacheStore.Get(GlobalApiCacheStore.MidTermLifetime, f, w, r, params)
	} else {
		GlobalApiCacheStore.Get(GlobalApiCacheStore.IgnoreCache, f, w, r, params)
	}
}

func jsonStats(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	var stats oapigen.StatsResponse
	var err error
	if statsJob != nil && statsJob.response.buf.Len() > 0 {
		err = json.Unmarshal(statsJob.response.buf.Bytes(), &stats)
	}
	if err != nil {
		respError(w, err)
		return
	}
	stats.RunePriceUSD = floatStr(stat.RunePriceUSD())
	respJSON(w, stats)
}

func jsonBalance(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	urlParams := r.URL.Query()

	height := util.ConsumeUrlParam(&urlParams, "height")
	timestamp := util.ConsumeUrlParam(&urlParams, "timestamp")

	if merr := util.CheckUrlEmpty(urlParams); merr != nil {
		merr.ReportHTTP(w)
		return
	}

	address := ps[0].Value
	result, merr := timeseries.GetBalances(r.Context(), address, height, timestamp)

	if merr != nil {
		merr.ReportHTTP(w)
		return
	}

	respJSON(w, result)
}

func jsonSwagger(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	swagger, err := oapigen.GetSwagger()
	if err != nil {
		respError(w, err)
		return
	}
	respJSON(w, swagger)
}

func writeJSON(w io.Writer, body interface{}) {
	e := json.NewEncoder(w)
	e.SetIndent("", "\t")
	// Error discarded
	_ = e.Encode(body)
}

func respJSON(w http.ResponseWriter, body interface{}) {
	w.Header().Set("Content-Type", "application/json")
	writeJSON(w, body)
}

func respError(w http.ResponseWriter, err error) {
	http.Error(w, err.Error(), http.StatusInternalServerError)
}

func ratioStr(a, b int64) string {
	if b == 0 {
		return "0"
	} else {
		return strconv.FormatFloat(float64(a)/float64(b), 'f', -1, 64)
	}
}

func floatStr(f float64) string {
	return strconv.FormatFloat(f, 'f', -1, 64)
}

// returns max 2 results
func withLowered(s string) []string {
	lower := strings.ToLower(s)
	if lower != s {
		return []string{s, lower}
	} else {
		return []string{s}
	}
}
