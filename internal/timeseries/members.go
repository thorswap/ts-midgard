package timeseries

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/lib/pq"

	"github.com/rs/zerolog/log"
	"gitlab.com/thorchain/midgard/config"

	"gitlab.com/thorchain/midgard/internal/db"
	"gitlab.com/thorchain/midgard/internal/fetch/record"
	"gitlab.com/thorchain/midgard/internal/util"
	"gitlab.com/thorchain/midgard/openapi/generated/oapigen"
)

// GetMemberIds returns the ids of all known members.
//
// The id of a member is defined as their rune address if they are participating with their rune
// address, or as their asset address otherwise (for members with asset address only.)
//
// Member ids present in multiple pools will be only returned once.
func GetMemberIds(ctx context.Context, pool *string) (addrs []string, err error) {
	poolFilter := ""

	qargs := []interface{}{}
	if pool != nil {
		poolFilter = "pool = $1"

		qargs = append(qargs, pool)
	}

	q := "SELECT DISTINCT member_id FROM midgard_agg.members " + db.Where(poolFilter)

	rows, err := db.Query(ctx, q, qargs...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var member string
		err := rows.Scan(&member)
		if err != nil {
			return nil, err
		}
		addrs = append(addrs, member)
	}

	return addrs, nil
}

func GetBorrowerIds(ctx context.Context, asset *string) (addrs []string, err error) {
	assetFilter := ""
	qargs := []interface{}{}
	if asset != nil {
		assetFilter = "collateral_asset = $1"
		qargs = append(qargs, asset)
	}

	q := "SELECT DISTINCT borrower_id FROM midgard_agg.borrowers " + db.Where(assetFilter)

	rows, err := db.Query(ctx, q, qargs...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var borrower string
		err := rows.Scan(&borrower)
		if err != nil {
			return nil, err
		}
		addrs = append(addrs, borrower)
	}

	return addrs, nil
}

// TODO(HooriRn): this struct might not be needed since the graphql depracation. (delete-graphql)
// Info of a member in a specific pool.
type MemberPool struct {
	Pool           string
	RuneAddress    string
	AssetAddress   string
	LiquidityUnits int64
	AssetDeposit   int64
	RuneDeposit    int64
	RuneAdded      int64
	AssetAdded     int64
	RunePending    int64
	AssetPending   int64
	DateFirstAdded int64
	DateLastAdded  int64
	RuneWithdrawn  int64
	AssetWithdrawn int64
}

// Info of a member in a specific pool.
type LPDetail struct {
	Pool              string
	LiquidityUnits    int64
	RuneAdded         int64
	AssetAdded        int64
	RuneWithdrawn     int64
	AssetWithdrawn    int64
	Date              int64
	Height            int64
	RunePriceUsd      float64
	AssetPriceUsd     float64
	AssetDepth        int64
	RuneDepth         int64
	PoolUnit          int64
	SharedUnit        int64
	AssetLiquidityFee int64
	RuneLiquidityFee  int64
	LiquidityFeeUsd   int64
	BlockRewards      int64
	AssetFee          float64
	RuneFee           float64
	Reward            float64
}

type SumUnit struct {
	Unit int64 `json:"unit"`
}

type PoolInfo struct {
	Pool      string  `json:"pool"`
	Date      int64   `json:"date"`
	PriceUsed float64 `json:"price_used"`
	AssetE8   int64   `json:"asset_e_8"`
	RuneE8    int64   `json:"rune_e_8"`
	Unit      int64   `json:"unit"`
}

func (memberPool MemberPool) toOapigen() oapigen.MemberPool {
	return oapigen.MemberPool{
		Pool:           memberPool.Pool,
		RuneAddress:    memberPool.RuneAddress,
		AssetAddress:   memberPool.AssetAddress,
		LiquidityUnits: util.IntStr(memberPool.LiquidityUnits),
		RuneDeposit:    util.IntStr(memberPool.RuneDeposit),
		AssetDeposit:   util.IntStr(memberPool.AssetDeposit),
		RuneAdded:      util.IntStr(memberPool.RuneAdded),
		AssetAdded:     util.IntStr(memberPool.AssetAdded),
		RuneWithdrawn:  util.IntStr(memberPool.RuneWithdrawn),
		AssetWithdrawn: util.IntStr(memberPool.AssetWithdrawn),
		RunePending:    util.IntStr(memberPool.RunePending),
		AssetPending:   util.IntStr(memberPool.AssetPending),
		DateFirstAdded: util.IntStr(memberPool.DateFirstAdded),
		DateLastAdded:  util.IntStr(memberPool.DateLastAdded),
	}
}

func (memberPool MemberPool) toSavers() oapigen.SaverPool {
	return oapigen.SaverPool{
		Pool:           util.ConvertSynthPoolToNative(memberPool.Pool),
		AssetAddress:   memberPool.AssetAddress,
		AssetAdded:     util.IntStr(memberPool.AssetAdded),
		AssetDeposit:   util.IntStr(memberPool.AssetDeposit),
		SaverUnits:     util.IntStr(memberPool.LiquidityUnits),
		AssetWithdrawn: util.IntStr(memberPool.AssetWithdrawn),
		DateFirstAdded: util.IntStr(memberPool.DateFirstAdded),
		DateLastAdded:  util.IntStr(memberPool.DateLastAdded),
	}
}

// Pools data associated with a single member
type MemberPools []MemberPool

func (memberPools MemberPools) ToOapigen() []oapigen.MemberPool {
	ret := make([]oapigen.MemberPool, len(memberPools))
	for i, memberPool := range memberPools {
		ret[i] = memberPool.toOapigen()
	}

	return ret
}

var (
	BaseURL string
	Client  http.Client
)

type ThorNodeMemberPool struct {
	Asset             string `json:"asset"`
	RuneAddress       string `json:"rune_address"`
	AssetAddress      string `json:"asset_address"`
	LastAddHeight     int    `json:"last_add_height"`
	Units             int64  `json:"units,string"`
	PendingRune       int64  `json:"pending_rune,string"`
	PendingAsset      int64  `json:"pending_asset,string"`
	RuneDepositValue  string `json:"rune_deposit_value"`
	AssetDepositValue string `json:"asset_deposit_value"`
}

func CheckPools(memberPools MemberPools) (MemberPools, error) {
	if BaseURL == "" {
		BaseURL = config.Global.ThorChain.ThorNodeURL
	}
	for i, memberPool := range memberPools {
		addr := memberPool.RuneAddress
		if memberPool.RuneAddress == "" {
			addr = memberPool.AssetAddress
		}
		resp, err := Client.Get(BaseURL + "/pool/" + memberPool.Pool + "/liquidity_provider/" + addr)
		if err != nil {
			log.Error().Err(err)
			continue
		}
		if resp.StatusCode/100 != 2 {
			log.Error().Err(err)
			continue
		}
		var thorNodeMemberPool ThorNodeMemberPool
		if err := json.NewDecoder(resp.Body).Decode(&thorNodeMemberPool); err != nil {
			log.Error().Err(err)
			continue
		}
		if thorNodeMemberPool.Asset == "" {
			log.Error().Msg("Invalid pool")
			continue
		}
		memberPools[i].AssetPending = thorNodeMemberPool.PendingAsset
		memberPools[i].RunePending = thorNodeMemberPool.PendingRune
		memberPools[i].LiquidityUnits = thorNodeMemberPool.Units
	}
	return memberPools, nil
}

func (memberPools MemberPools) ToSavers(poolRedeemValueMap map[string]int64) []oapigen.SaverPool {
	ret := make([]oapigen.SaverPool, len(memberPools))
	for i, memberPool := range memberPools {
		ret[i] = memberPool.toSavers()
		ret[i].AssetRedeem = util.IntStr(poolRedeemValueMap[memberPool.Pool])
	}

	return ret
}

type MemberPoolType int

const (
	RegularAndSaverPools MemberPoolType = iota // regular and synth pools too
	RegularPools                               // regular (non-synth) pools e.g. 'BTC.BTC'
	SaverPools                                 // LPs of synth pools e.g. 'BTC/BTC'
)

func PoolBasedOfType(poolName string, poolType MemberPoolType) bool {
	if poolType == RegularAndSaverPools {
		return true
	}
	poolCoinType := record.GetCoinType([]byte(poolName))
	if poolCoinType == record.AssetSynth && poolType == SaverPools {
		return true
	}
	if poolCoinType == record.AssetNative && poolType == RegularPools {
		return true
	}
	return false
}

func GetMemberPools(ctx context.Context, address []string, poolType MemberPoolType) (MemberPools, error) {
	q := `
		SELECT
			pool,
			COALESCE(rune_addr, ''),
			COALESCE(asset_addr, ''),
			lp_units_total,
			asset_e8_deposit,
			rune_e8_deposit,
			added_rune_e8_total,
			added_asset_e8_total,
			withdrawn_rune_e8_total,
			withdrawn_asset_e8_total,
			pending_rune_e8_total,
			pending_asset_e8_total,
			COALESCE(first_added_timestamp / 1000000000, 0),
			COALESCE(last_added_timestamp / 1000000000, 0)
		FROM midgard_agg.members
		WHERE member_id = ANY($1) OR asset_addr = ANY($1)
		ORDER BY pool`
	rows, err := db.Query(ctx, q, pq.Array(address))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var results MemberPools
	for rows.Next() {
		var entry MemberPool
		err := rows.Scan(
			&entry.Pool,
			&entry.RuneAddress,
			&entry.AssetAddress,
			&entry.LiquidityUnits,
			&entry.AssetDeposit,
			&entry.RuneDeposit,
			&entry.RuneAdded,
			&entry.AssetAdded,
			&entry.RuneWithdrawn,
			&entry.AssetWithdrawn,
			&entry.RunePending,
			&entry.AssetPending,
			&entry.DateFirstAdded,
			&entry.DateLastAdded,
		)
		if err != nil {
			return nil, err
		}
		if PoolBasedOfType(entry.Pool, poolType) {
			results = append(results, entry)
		}
	}
	return results, nil
}

func GetFullMemberPools(ctx context.Context, address string) (MemberPools, error) {
	if record.AddressIsRune(address) {
		return GetMemberPools(ctx, []string{address}, RegularPools)
	} else {
		memberPools, err := GetMemberPools(ctx, []string{address}, RegularPools)
		if err != nil {
			return memberPools, err
		}
		var runAddr string
		for _, memberPool := range memberPools {
			if memberPool.RuneAddress != "" {
				runAddr = memberPool.RuneAddress
			}
		}
		if runAddr != "" {
			runePools, err := GetMemberPools(ctx, []string{runAddr}, RegularPools)
			if err != nil {
				return memberPools, err
			}
			for _, p1 := range runePools {
				exits := false
				for _, p2 := range memberPools {
					if p1.Pool == p2.Pool && p1.RuneAddress == p2.RuneAddress && p1.AssetAddress == p2.AssetAddress {
						exits = true
						break
					}
				}
				if !exits {
					memberPools = append(memberPools, p1)
				}
			}
		}
		return memberPools, err
	}
}

func GetSumPoolUnits(ctx context.Context, pool string, date int64, tableName string) (*int64, *error) {
	q := `select sum(stake_units) from ` + tableName + ` where pool=$1 and block_timestamp<=$2`

	rows, err := db.Query(ctx, q, pool, date)
	if err != nil {
		return nil, &err
	}
	defer rows.Close()

	var sumUnit int64 = 0
	for rows.Next() {
		var entry SumUnit
		err := rows.Scan(
			&entry.Unit,
		)
		if err != nil {
			return nil, &err
		}
		sumUnit += entry.Unit
	}
	return &sumUnit, nil
}

func HotFixSumPoolUnit(ctx context.Context, pool string, lpDetails LPDetail) (*int64, *error) {
	var stack_event_unit int64 = 0
	var unStack_event_unit int64 = 0

	stakeUnit, err := GetSumPoolUnits(ctx, pool, lpDetails.Date, "stake_events")
	if err != nil {
		return nil, err
	}
	if stakeUnit != nil {
		stack_event_unit += *stakeUnit
	}

	unStakUnit, errr := GetSumPoolUnits(ctx, pool, lpDetails.Date, "withdraw_events")
	if err != nil {
		return nil, errr
	}
	if unStakUnit != nil {
		unStack_event_unit += *unStakUnit
	}

	returnValue := stack_event_unit - unStack_event_unit
	return &returnValue, nil
}

func GetLpDetail(ctx context.Context, runeAddress, assetAddress, pool string) ([]LPDetail, error) {
	lpDetails, err := lpDetailsRune(ctx, runeAddress, assetAddress, pool)
	if err != nil {
		return nil, err
	}
	if len(lpDetails) == 0 {
		return lpDetails, nil
	}
	lpDetails, err = getBlockHeight(ctx, lpDetails)
	if err != nil {
		return nil, err
	}

	for i, items := range lpDetails {
		if items.Height > 7960000 {
			sumUnit, err := HotFixSumPoolUnit(ctx, pool, items)
			if err != nil {
				return nil, *err
			}
			lpDetails[i].PoolUnit = *sumUnit
		}
	}

	for i, lp := range lpDetails {
		lpDetails[i].Date = lp.Date / 1000000000
	}

	lpDetails, err = poolInfo(ctx, pool, lpDetails)
	if err != nil {
		return nil, err
	}

	return lpDetails, nil
}

const lpAddLiquidityQFields = `
		asset_E8,
		rune_E8,
		stake_units,
		block_timestamp,
		pool_asset_fee_per_unit,
		pool_rune_fee_per_unit,
		pool_rune_reward_per_unit
`

const lpWithdrawQFields = `
		emit_asset_e8,
		emit_rune_e8,
		stake_units,
		block_timestamp,
		pool_asset_fee_per_unit,
		pool_rune_fee_per_unit,
		pool_rune_reward_per_unit
`

func lpDetailsRune(ctx context.Context, runeAddress, assetAddress, pool string) ([]LPDetail, error) {
	addLiquidityQ := `SELECT
		pool,
	` + lpAddLiquidityQFields + `
	FROM stake_events
	`
	qargs := make([]interface{}, 0)
	if runeAddress == "" {
		addLiquidityQ += `
						WHERE asset_addr = $1
						AND rune_addr IS NULL
						AND pool = $2
						AND (asset_E8 != 0 OR rune_E8 != 0)
						`
		qargs = append(qargs, assetAddress, pool)
	} else if assetAddress == "" {
		addLiquidityQ += `
						WHERE rune_addr = $1
						AND asset_addr IS NULL
						AND pool = $2
						AND (asset_E8 != 0 OR rune_E8 != 0)
						`
		qargs = append(qargs, runeAddress, pool)
	} else {
		addLiquidityQ += `
						WHERE rune_addr = $1
						AND pool = $2
						AND (asset_E8 != 0 OR rune_E8 != 0)
						`
		qargs = append(qargs, runeAddress, pool)
	}

	addLiquidityRows, err := db.Query(ctx, addLiquidityQ, qargs...)
	if err != nil {
		return nil, err
	}
	defer addLiquidityRows.Close()

	lpDetails := make([]LPDetail, 0)
	for addLiquidityRows.Next() {
		lpDetail := LPDetail{}
		err := addLiquidityRows.Scan(
			&lpDetail.Pool,
			&lpDetail.AssetAdded,
			&lpDetail.RuneAdded,
			&lpDetail.LiquidityUnits,
			&lpDetail.Date,
			&lpDetail.AssetFee,
			&lpDetail.RuneFee,
			&lpDetail.Reward,
		)
		if err != nil {
			return nil, err
		}
		lpDetails = append(lpDetails, lpDetail)
	}

	withdrawQ := `SELECT
		pool,
		` + lpWithdrawQFields + `
	FROM withdraw_events
	WHERE from_addr = $1
	AND pool = $2
	AND (emit_asset_e8 != 0 OR emit_rune_e8 != 0)
	`
	addr := runeAddress
	if addr == "" {
		addr = assetAddress
	}
	withdrawRows, err := db.Query(ctx, withdrawQ, addr, pool)
	if err != nil {
		return nil, err
	}
	defer withdrawRows.Close()

	for withdrawRows.Next() {
		var pool string
		var assetWithdrawn, runeWithdrawn, unitsWithdrawn, date int64
		var assetFee, runeFee, runeReward float64
		err := withdrawRows.Scan(&pool, &assetWithdrawn, &runeWithdrawn, &unitsWithdrawn, &date, &assetFee, &runeFee, &runeReward)
		if err != nil {
			return nil, err
		}
		lpDetail := LPDetail{
			LiquidityUnits: -unitsWithdrawn,
			RuneWithdrawn:  runeWithdrawn,
			AssetWithdrawn: assetWithdrawn,
			Date:           date,
			AssetFee:       assetFee,
			RuneFee:        runeFee,
			Reward:         runeReward,
		}
		lpDetails = append(lpDetails, lpDetail)
	}
	return lpDetails, nil
}

func getBlockHeight(ctx context.Context, lpDetails []LPDetail) ([]LPDetail, error) {
	dates := make([]int64, 0)
	for _, lp := range lpDetails {
		dates = append(dates, lp.Date)
	}
	datesStr := ""
	for _, d := range dates {
		if len(datesStr) != 0 {
			datesStr += ","
		}
		datesStr += strconv.FormatInt(d, 10)
	}
	blockInfoQ := fmt.Sprintf(`SELECT  height,timestamp
				FROM   block_log
				WHERE  timestamp in (%s) `, datesStr)
	blockInfoRows, err := db.Query(ctx, blockInfoQ)
	if err != nil {
		return nil, err
	}
	defer blockInfoRows.Close()
	var height, timestamp int64
	for blockInfoRows.Next() {
		err := blockInfoRows.Scan(&height, &timestamp)
		if err != nil {
			return nil, err
		}
		for i, lp := range lpDetails {
			if lp.Date == timestamp {
				lpDetails[i].Height = height
			}
		}
	}
	return lpDetails, nil
}

func poolInfo(ctx context.Context, pool string, lpDetails []LPDetail) ([]LPDetail, error) {
	dates := make([]int64, 0)
	for _, lp := range lpDetails {
		dates = append(dates, int64(float64(lp.Date)/(60*5))*(60*5)*(1e+9))
	}
	datesStr := ""
	for _, d := range dates {
		if len(datesStr) != 0 {
			datesStr += ","
		}
		datesStr += strconv.FormatInt(d, 10)
	}
	poolInfoQ := fmt.Sprintf(`SELECT  pool,aggregate_timestamp,priceusd,asset_E8,rune_E8,units
				FROM   midgard_agg.pool_depths_5min
				WHERE  pool = $1
					   AND aggregate_timestamp in (%s) `, datesStr)

	poolInfoRows, err := db.Query(ctx, poolInfoQ, pool)
	if err != nil {
		return nil, err
	}
	defer poolInfoRows.Close()
	var poolInfo PoolInfo
	poolInfos := make([]PoolInfo, 0)
	for poolInfoRows.Next() {
		err := poolInfoRows.Scan(&poolInfo.Pool, &poolInfo.Date, &poolInfo.PriceUsed, &poolInfo.AssetE8, &poolInfo.RuneE8, &poolInfo.Unit)
		if err != nil {
			return nil, err
		}
		poolInfos = append(poolInfos, poolInfo)
	}
	for i, poolDetail := range lpDetails {
		for _, poolInfo := range poolInfos {
			if int64(float64(poolDetail.Date/(60*5))*(60*5)*(1e+9)) == poolInfo.Date {
				lpDetails[i].AssetPriceUsd = poolInfo.PriceUsed
				lpDetails[i].RunePriceUsd = lpDetails[i].AssetPriceUsd / (float64(poolInfo.RuneE8) / float64(poolInfo.AssetE8))
				lpDetails[i].AssetDepth = poolInfo.AssetE8
				lpDetails[i].RuneDepth = poolInfo.RuneE8
				// commented for poolUnit Hotfix
				if lpDetails[i].PoolUnit == 0 {
					lpDetails[i].PoolUnit = poolInfo.Unit
				}
			}
		}
	}
	return lpDetails, nil
}
