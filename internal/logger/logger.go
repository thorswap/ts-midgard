package logger

import (
	"sync"
	"time"
)

var (
	logLock *sync.Mutex
	AllLogs map[string]string
)

func init() {
	logLock = new(sync.Mutex)
	AllLogs = make(map[string]string)
}

func AddLog(log string) {
	return
	logLock.Lock()
	defer logLock.Unlock()
	if len(AllLogs) > 3000 {
		AllLogs = make(map[string]string)
	}
	AllLogs[time.Now().String()] = log
}

func GetLogs() map[string]string {
	return AllLogs
	logLock.Lock()
	defer logLock.Unlock()
	result := make(map[string]string)
	for k, v := range AllLogs {
		result[k] = v
	}
	return result
}
